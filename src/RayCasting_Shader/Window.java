package RayCasting_Shader;

import Voxels.GrayVoxel;
import com.jogamp.opengl.util.Animator;

import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import java.awt.*;
import java.awt.event.*;
import Render.FPSMeter;
import static java.awt.event.KeyEvent.*;

public class Window extends Frame
{
	private Animator animator;
	private Camera camera;
	private Render render;
	int x,y;

    public Window()
	{
        GLCanvas canvas = new GLCanvas();
		canvas.setSize(640, 480);
		GLProfile profile = canvas.getGLProfile();
		setTitle("Camera simple");
		setResizable(true);
		setLayout(new BorderLayout());
		add(canvas, BorderLayout.CENTER);
		setSize(getPreferredSize());

		camera = new Camera(4, 4, 4, 0, 0, 0, 0, 1, 0,this.getHeight(),this.getWidth());
		render = new Render(profile, camera);

		canvas.addGLEventListener(render);
		canvas.addGLEventListener(new FPSMeter());

        canvas.addKeyListener(new KeyListener()
        {
            @Override
            public void keyTyped(KeyEvent e)
            {
            }

            @Override
            public void keyPressed(KeyEvent ke)
            {
                switch (ke.getKeyCode())
                {
                    case VK_PLUS:
                        camera.zoom(1.1f);
                        break;
                    case VK_MINUS:
                        camera.zoom(0.9f);
                        break;
                    case VK_DOWN: case VK_S:
                        camera.move(0.02f);
                        break;
                    case VK_UP: case VK_W :
                        camera.move(-0.02f);
                        break;
                    case VK_D: case VK_RIGHT:
                        camera.strafe(0.02f);
                        break;
                    case VK_A: case VK_LEFT:
                        camera.strafe(-0.02f);
                        break;
                    case VK_Q:
                        camera.strafeDown(-0.02f);
                        break;
                    case VK_E:
                        camera.strafeDown(0.02f);
                        break;
                    case VK_1:
                        render.model.levelDown();
                        break;
                    case VK_2:
                        render.model.levelUp();
                        break;
                    case VK_3:
                        render.MODEL_OF = !render.MODEL_OF;
                        break;
                    case VK_4:
                        GrayVoxel.DRAW_AVR_COLOR = !GrayVoxel.DRAW_AVR_COLOR;
                        break;
                    case VK_5:
                        GrayVoxel.DRAW_GRAY_VOXEL = !GrayVoxel.DRAW_GRAY_VOXEL;
                        break;
                }
                ke.getComponent().repaint();
            }

            @Override
            public void keyReleased(KeyEvent e)
            {
            }
        });

		canvas.addMouseMotionListener(new MouseMotionAdapter()
		{
			@Override
			public void mouseMoved(MouseEvent me)
			{
			}

			@Override
			public void mouseDragged(MouseEvent me)
			{
//				super.mouseDragged(me);
				if (x > me.getX())
				{
					x = me.getX();
					// System.out.println("Влево");
					camera.rotateAroundPoint(camera.mView, 0.1f, 0f, 1f, 0f);
				} else if (x < me.getX())
				{
					x = me.getX();
					//System.out.println("Вправо");
					camera.rotateAroundPoint(camera.mView, -0.1f, 0f, 1f, 0f);
				}

				if (y > me.getY())
				{
					y = me.getY();
					//System.out.println("Вверх");
				} else if (y < me.getY())
				{
					y = me.getY();
					// System.out.println("Вниз");
				}
			}
		});

		canvas.addMouseWheelListener(new MouseWheelListener()
		{
			@Override
			public void mouseWheelMoved(MouseWheelEvent me)
			{
				if (me.getUnitsToScroll() > 0)
				{
					camera.zoom(1.1f);
				} else
				{
					camera.zoom(0.9f);
				}
			}
		});

		animator = new Animator(canvas);
		animator.setRunAsFastAsPossible(true);
		animator.start();

		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				animator.stop();
				dispose();
			}
		});
	}
}