package RayCasting_Shader;

import Mesh.Vector;
import RayCasting.RenderCPU_Flat;
import Voxels.VoxelBuffer;
import Voxels.VoxelModel;
import Voxels.VoxelReader;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.*;
import javax.media.opengl.glu.GLU;

import static javax.media.opengl.GL2.*;

public class Render implements GLEventListener
{
    Camera camera;
    GLU glu;
    GLProfile profile;
    public VoxelModel model;
    RenderCPU_Flat render;
    VoxelBuffer bufferModel;
    GLUT glut;
    public boolean MODEL_OF;

    public Render(GLProfile profile, Camera camera)
    {
        this.camera = camera;
        this.profile = profile;
        try
        {
            VoxelReader reader = new VoxelReader();
            model = reader.read("octree/tor1.octree");
            System.out.println("Finish octree model read...");
//            FabricVoxelTerrain fabric = new FabricVoxelTerrain();
//            model = fabric.generic(1., new Vector(-0.5, -0.5, 0.5), (byte) 4);

            model.setPoint(new Vector(-0.5, -0.5, 0.5));
            glut = new GLUT();
        } catch (Exception e)
        {
            e.printStackTrace();
            System.exit(11);
        }
    }

    @Override
    public void init(GLAutoDrawable drawable)
    {
        GL2 gl = drawable.getGL().getGL2();
        glu = new GLU();
        gl.glClearColor(1f, 1f, 1f, 0f);
        gl.glMatrixMode(GL_MODELVIEW);

        gl.glEnable(GL_DEPTH_TEST);
        gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL_BLEND);

        gl.glEnable(GL_ALPHA_TEST);
        gl.glAlphaFunc(GL_LESS, 0.5f);
//        gl.glDepthFunc(GL_GREATER);

    }

    @Override
    public void display(GLAutoDrawable drawable)
    {
        GL2 gl = drawable.getGL().getGL2();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        camera.look(glu);

        if(!MODEL_OF)
             model.draw(gl, glut);

        gl.glPushMatrix();
        double size = model.size / 2.;
        gl.glTranslated(model.point.x + size, model.point.y + size, model.point.z + size);
        gl.glColor4d(0, 1, 0, 1);
        glut.glutWireCube((float) size * 2);
        gl.glPopMatrix();

        gl.glLoadIdentity();
    }

    @Override
    public void reshape(GLAutoDrawable glDrawable, int x, int y, int w, int h)
    {
        if (h == 0) h = 1;
        GL2 gl = glDrawable.getGL().getGL2();
        GLU glu = new GLU();
        gl.glViewport(0, 0, w, h);
        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f,(double) w / h, 0.001f, 600);
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    @Override
    public void dispose(GLAutoDrawable drawable)
    {
    }
}