package RayCasting_Shader;

import Mesh.Vector;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

/**
 *
 * @author thinker
 */

public class Camera
{
    public Vector mPos, mView, mUp;
    boolean Debug;
    int height, widht;
    public Camera(float pos_x, float pos_y, float pos_z, float view_x, float view_y, float view_z,
		  float up_x, float up_y, float up_z, int height, int widht)
    {
        setPosition(pos_x, pos_y, pos_z, view_x, view_y, view_z, up_x, up_y, up_z);
        Debug = true;
        this.height = height;
        this.widht = widht;
    }

	public void setPosition(float pos_x, float pos_y, float pos_z, float view_x, float view_y, float view_z,
	                        float up_x, float up_y, float up_z)
	{
		mPos = new Vector(pos_x,  pos_y,  pos_z);
		mView = new Vector(view_x, view_y, view_z);
		mUp = new Vector(up_x,up_y,up_z);
	}

	public void move(float speed)
	{
		Vector vector = mPos.subtract(mView);
		mPos.add(vector.x * speed,  0, vector.z * speed);
		mView.add(vector.x * speed, 0, vector.z * speed);
	}

	public void strafe(float speed)
	{
		rotateAroundPoint(mView, (float)(Math.PI / 2.), 0f, 1f, 0f);
		Vector vector = mPos.subtract(mView);

		mPos.add(vector.x * speed,  0, vector.z * speed);
		mView.add(vector.x * speed, 0, vector.z * speed);
		rotateAroundPoint(mView, -(float)(Math.PI / 2.), 0f, 1f, 0f);
	}

    public void strafeDown(float speed)
    {
        Vector vector = mPos.subtract(mView);
        mPos.add(0,  vector.y * speed, 0);
        mView.add(0, vector.y * speed, 0);
    }

	public void rotateView(float angle, float x, float y, float z)
	{
	 	Vector vView = mView.subtract(mPos);
		float cosTheta = (float)Math.cos(angle);
		float sinTheta = (float)Math.sin(angle);

		Vector vNewView = new Vector();
		vNewView.x  = (cosTheta + (1 - cosTheta) * x * x)		* vView.x;
		vNewView.x += ((1 - cosTheta) * x * y - z * sinTheta)	* vView.y;
		vNewView.x += ((1 - cosTheta) * x * z + y * sinTheta)	* vView.z;

		vNewView.y  = ((1 - cosTheta) * x * y + z * sinTheta)	* vView.x;
		vNewView.y += (cosTheta + (1 - cosTheta) * y * y)		* vView.y;
		vNewView.y += ((1 - cosTheta) * y * z - x * sinTheta)	* vView.z;

		vNewView.z  = ((1 - cosTheta) * x * z - y * sinTheta)	* vView.x;
		vNewView.z += ((1 - cosTheta) * y * z + x * sinTheta)	* vView.y;
		vNewView.z += (cosTheta + (1 - cosTheta) * z * z)		* vView.z;

		mView = mPos.add(vNewView);
	}

	public void rotateAroundPoint(Vector center, float angle, float x, float y, float z)
	{
		Vector newPos = new Vector(),
				 vPos = mPos.subtract(center);
		float cosTheta = (float)Math.cos(angle);
		float sinTheta = (float)Math.sin(angle);

		newPos.x  = (cosTheta + (1 - cosTheta) * x * x)		* vPos.x;
		newPos.x += ((1 - cosTheta) * x * y - z * sinTheta)	* vPos.y;
		newPos.x += ((1 - cosTheta) * x * z + y * sinTheta)	* vPos.z;

		newPos.y  = ((1 - cosTheta) * x * y + z * sinTheta)	* vPos.x;
		newPos.y += (cosTheta + (1 - cosTheta) * y * y)		* vPos.y;
		newPos.y += ((1 - cosTheta) * y * z - x * sinTheta)	* vPos.z;

		newPos.z  = ((1 - cosTheta) * x * z - y * sinTheta)	* vPos.x;
		newPos.z += ((1 - cosTheta) * y * z + x * sinTheta)	* vPos.y;
		newPos.z += (cosTheta + (1 - cosTheta) * z * z)		* vPos.z;

		mPos = center.add(newPos);
	}

	public void zoom(float speed)
	{
		Vector vVector = mPos.subtract(mView);
	        if(speed>1.0f)
	        {
	            if((Math.abs(vVector.x)<400)&&
	               (Math.abs(vVector.y)<400)&&(Math.abs(vVector.z)<400))
	            {
	                mPos.x = mView.x + vVector.x*speed;
	                mPos.y = mView.y + vVector.y*speed;
	                mPos.z = mView.z + vVector.z*speed;
	            }
	       }
	       else
	       {
	            if((Math.abs(vVector.x)>0)&&
	               (Math.abs(vVector.y)>0)&&(Math.abs(vVector.z)>0))
	            {
	                mPos.x = mView.x + vVector.x*speed;
	                mPos.y = mView.y + vVector.y*speed;
	                mPos.z = mView.z + vVector.z*speed;
	            }
	       }
	}


    public void debug(GL2 gl)
    {
        gl.glPushMatrix();
        GLUT glut = new GLUT();
	    gl.glColor3f(0f, 0f, 0f);
        gl.glTranslated(mView.x,mView.y,mView.z);

	    glut.glutSolidTeapot(1.0);
//	    gl.glLoadMatrixd();
        gl.glPopMatrix();
    }
    
    public void look(GLU glu)
    {
        glu.gluLookAt(mPos.x, mPos.y, mPos.z,
                      mView.x, mView.y, mView.z,
                      mUp.x, mUp.y, mUp.z);

    }
}   
    
