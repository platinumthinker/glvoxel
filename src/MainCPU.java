import Mesh.Vector;
import RayCasting.RenderCPU_Flat;
import Voxels.VoxelModel;
import Voxels.VoxelReader;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 24.11.12
 * Time: 13:26
 */
public class MainCPU
{
    public static void main(String[] args)
    {
        try
        {
            VoxelReader reader = new VoxelReader();
            VoxelModel model = reader.read("octree/torus_knot.octree");
            System.out.println("Finish read...");
            model.setPoint(new Vector(-0.5, -0.5, 0.5));
//            RenderCPU render = new RenderCPU(model);
            RenderCPU_Flat render = new RenderCPU_Flat(model);
        } catch (IOException e)
        {
            e.printStackTrace();
            System.exit(11);
        }
    }
}

//gpu
//574

//cpu
//1600
//1563
//1197