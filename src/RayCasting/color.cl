//#pragma OPENCL_EXTENSION cl_khr_local_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_fp64: enable

double isContains(double3 dir, double3 origin, __local double3 bounds[2])
{
    __private double min, max, min1, max1;
    __private uchar3 sign;

    sign.x = (dir.x < 0) ? 1 : 0;
    sign.y = (dir.y < 0) ? 1 : 0;
    sign.z = (dir.z < 0) ? 1 : 0;

    min =  (bounds[sign.x].x   - origin.x) /dir.x;
    max =  (bounds[1-sign.x].x - origin.x) /dir.x;
    min1 = (bounds[sign.y].y   - origin.y) /dir.y;
    max1 = (bounds[1-sign.y].y - origin.y) /dir.y;

    if ( (min > max1) || (min1 > max) )
        return NAN;

    if (min1 > min)
        min = min1;

    if (max1 < max)
        max = max1;

    min1 = (bounds[sign.z].z   - origin.z) /dir.z;
    max1 = (bounds[1-sign.z].z - origin.z) /dir.z;

    if ( (min > max1) || (min1 > max) )
        return NAN;

    if (min1 > min)
        min = min1;

    if (max1 < max)
        max = max1;

    if ((min < max) && (max > 0.f))
        return min * EPS;
    else
        return NAN;
}


kernel void color(global int* color_buffer,
                  global double* depth_buffer,
                  global double* rays,
                  global char* child,
                  global int* voxels,
                  global double* camera_pos_ptr,
                  global double* model_pos_ptr
                  )
{
     __private int ind = get_global_id(0);

//     __private uint x = ind % WIDTH;
//     __private uint y = ind / WIDTH;

     __local double3 camera_pos, m[2];

//     if(ind % 25 == 0)
     {
        camera_pos = vload3(0, camera_pos_ptr);
        m[0] = vload3(0, model_pos_ptr);
        m[1] = m[0] + SIZE;
     }

      __private double3 dir = camera_pos + vload3(ind, rays);

     __private double d = isContains(dir, camera_pos, m);
     
     if(d != NAN)
     {
        __private double3 c = (d * (dir - camera_pos) + camera_pos);
        __private double3 delta = dir * 0.5 / SIZE;
        __private double3 f = (c - m[0]) * SIZE * 2.0;
        __private int i, n;

        for(i = 1, n = 2; i < LEVEL; i++, n <<= 1)
        {
            delta *= 0.5;

            while(f.x > -1 && f.x + 1 < n &&
                  f.y > -1 && f.y + 1 < n &&
                  f.z > -1 && f.z + 1 < n &&
                  !child[( (i * n + ((int) f.x) ) * n + ((int) f.y) ) * n +  ((int) f.z)])
                    f += delta;

            f *= 2.;
        }

         while( f.x > -1 && f.x + 1 < N &&
              f.y > -1 && f.y + 1 < N &&
              f.z > -1 && f.z + 1 < N &&
              !voxels[((int) f.x * N + (int) f.y) * N + (int) f.z ])
                f += delta;

         if(f.x < 0 || f.x + 2 > N
            || f.y < 0 || f.y + 2 > N
            || f.z < 0 || f.z + 2 > N ||
            !voxels[((int) f.x * N + (int) f.y) * N + (int) f.z ])
            {
                color_buffer[ind * 3]     = 0;
                color_buffer[ind * 3 + 1] = 0;
                color_buffer[ind * 3 + 2] = 0;
                return;
            }

         __private int color = voxels[((int) f.x * N + (int) f.y) * N + (int) f.z ];
//        depth_buffer[x][y] = (float) ( (f * SIZE) + camera_pos[0]).magnitude() +  buffer.size*(buffer.colorBuffer[x1][y1][z1] & 0xff) / 255.);
//        red channel
        color_buffer[ind * 3]     = color >> 24 & 0xFF;
//        green channel
        color_buffer[ind * 3 + 1] = color >> 16 & 0xFF;
//        blue channel
        color_buffer[ind * 3 + 2] = color >>  8 & 0xFF;

     }
     else
     {
//         depth_buffer[x][y] = Float.NaN; CL_NAN
         color_buffer[ind * 3]     = 0;
         color_buffer[ind * 3 + 1] = 0;
         color_buffer[ind * 3 + 2] = 0;
     }
}

