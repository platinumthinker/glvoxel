package RayCasting;

import Mesh.Vector;
import Voxels.*;

import javax.media.opengl.GL2;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 17.10.12
 * Time: 22:24
 */

public class RenderCPU_Flat extends JFrame
{
	Camera camera;
	Vector pos;
	int height, width;
	VoxelModel model;
    private int[] pixels;
//	byte color_buffer[][][];
    float depth_buffer[][];
    float normal_buffer[][];
    VoxelBufferFlat buffer;
    OmniLight light;

    ArrayList<Vector> intersection;
    private BufferedImage bufferedImage;
    boolean DEPTH_BUFFER_ON = false;

    public RenderCPU_Flat(VoxelModel model)
	{
		this.camera = new Camera(500, 500, new Vector(0, 0, 0));

        pos = camera.pos.clone();
		height = camera.height;
		width = camera.width;
		this.model = model;

        pixels = new int[height * width * 3];

        bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        intersection = new ArrayList<>();

//		color_buffer = new byte[width][height][4];
        depth_buffer = new float[width][height];
        normal_buffer = new float[width][height];

        light = new OmniLight();
        light.point = new Vector(0,1,1);
        light.color = new Color(255, 253, 244);

        buffer = new VoxelBufferFlat(model);
        System.out.println("Finish buffering...");

        setSize(width, height);
        setLocation(700, 0);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        System.out.println("Start rendering....");
        calculate();

//        rayDebug(camera.getCoordinate(200, 250), 200, 250);
//        System.out.println(intersection.size());
    }

    public void draw(GL2 gl)
	{
		gl.glPointSize(0.3f);
		gl.glBegin(GL2.GL_POINTS);
		for(int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
			{
				Vector v = camera.getCoordinate(width - 1 - i, height - 1 - j);
				gl.glColor3ui(pixels[(j * width + i) * 3],
                              pixels[(j * width + i) * 3 + 1],
                              pixels[(j * width + i) * 3 + 2]);
				gl.glVertex3d(v.x / 7, v.y / 7, v.z / 7);
			}
		gl.glEnd();

//        gl.glColor3d(0, 1, 1);
//        gl.glPointSize(1.5f);

//        if(DEPTH_BUFFER_ON)
//        {
//            gl.glBegin(GL2.GL_POINTS);
//            for(Vector v : intersection)
//                gl.glVertex3d(v.x, v.y, v.z);
//            gl.glEnd();
//        }

        camera.draw(gl);
//        light.draw(gl, new GLUT());
	}

    private void rayDebug(Vector dir, int x, int y)
    {
        double size = model.size;
        Vector a = model.point.clone();
        Vector b = a.clone();
        b.add(size, size, size);
        Vector c = new Vector();
        if(isContains(dir, pos, new Vector[]{a, b}, c))
        {
            double d = c.z;

            c.z = d * (dir.z - pos.z) + pos.z;
            c.y = d * (dir.y - pos.y) + pos.y;
            c.x = d * (dir.x - pos.x) + pos.x;

            Vector f = c.subtract(a).multiply(size * 2);
//            intersection.add(f.divide(new Vector(size * 2)).add(a));

            Vector delta = dir.multiply(buffer.size * Math.pow(2, model.getLevel()));


            for(int i = 1, n = 2; i < model.getLevel(); i++, n <<= 1)
            {
                while(clampVector(f, -1, n) &&
                buffer.childBuffer[getIndex(f, i, n)] == 0)
                {
                    intersection.add(f.divide(new Vector(size * n )).add(a));
                    f.add(delta.x, delta.y, delta.z);
                }
//                delta.divide(2., 2., 2.);
                f.multiply(2, 2, 2);
            }

//            while(f.x > -1 && f.x + 1 < buffer.n &&
//                    f.y > -1 && f.y + 1 < buffer.n &&
//                    f.z > -1 && f.z + 1 < buffer.n &&
//                    buffer.colorBuffer[((int) f.x * buffer.n + (int) f.y) * buffer.n + (int) f.z ] == 0)
//            {
//                f.add(delta.x, delta.y, delta.z);
//                intersection.add(f.multiply(buffer.size).add(buffer.point));

//            }

            if(f.x < 0 || f.x + 2 > buffer.n
               || f.y < 0 || f.y + 2 > buffer.n
               || f.z < 0 || f.z + 2 > buffer.n ||
               buffer.colorBuffer[colorIndex((int) f.x, (int) f.y, (int) f.z)] == 0)
            {
                depth_buffer[x][y] = Float.NaN;
//                color_buffer[x][y] = new byte[]{0,0,0,0};
                pixels[(x * width + y) * 3    ] = 0;
                pixels[(x * width + y) * 3 + 1] = 0;
                pixels[(x * width + y) * 3 + 2] = 0;
                return;
            }

            int x1 = ((int) f.x), y1 = ((int) f.y), z1 = ((int) f.z);

            int color = buffer.colorBuffer[colorIndex(x1, y1, z1)];
            byte A, R, G, B;
            R = (byte) (color >> 24 & 0xFF);
            G = (byte) (color >> 16 & 0xFF);
            B = (byte) (color >> 8  & 0xFF);
            A = (byte) (color       & 0xFF);

//            color_buffer[x][y] = new byte[]{R, G, B, A};
            pixels[(x * width + y) * 3    ] = R;
            pixels[(x * width + y) * 3 + 1] = G;
            pixels[(x * width + y) * 3 + 2] = B;

        }
        else
        {
//            color_buffer[x][y] = new byte[]{0, 0, 0, 0};
            pixels[(x * width + y) * 3    ] = 0;
            pixels[(x * width + y) * 3 + 1] = 0;
            pixels[(x * width + y) * 3 + 2] = 0;
        }
    }

    int getIndex(Vector pos, int level, int n)
    {
        return ((level * n + ((int) pos.x)) * n + ((int) pos.y)) * n + ((int) pos.z);
    }

    Vector computeNormal(Vector pos, double d, int n, int i)
    {
        Vector posX = pos.clone();
        posX.x += d;
        int xP = buffer.colorBuffer[getIndex(posX, i, n)];
        posX.x -= 2*d;
        int xN = buffer.colorBuffer[getIndex(posX, i, n)];

        Vector posY = pos.clone();
        posY.y += d;
        int yP = buffer.colorBuffer[getIndex(posY, i, n)];
        posY.y -= 2*d;
        int yN = buffer.colorBuffer[getIndex(posY, i, n)];

        Vector posZ = pos.clone();
        posZ.z += d;
        int zP = buffer.colorBuffer[getIndex(posZ, i, n)];
        posZ.z -= 2*d;
        int zN = buffer.colorBuffer[getIndex(posZ, i, n)];

        Vector lNormal = new Vector(xN & 0xFF - xP & 0xFF, yN & 0xFF - yP & 0xFF, zN & 0xFF - zP & 0xFF);
        return lNormal.normalize();
    }

    private boolean clampVector(Vector f, int a, int b)
    {
        return f.x > a && f.x + 1 < b &&
               f.y > a && f.y + 1 < b &&
               f.z > a && f.z + 1 < b;
    }

    private void calculate()
	{
        long time = System.currentTimeMillis();

        ArrayList<Thread> trGroup = new ArrayList<>();

        for(int i = 0; i < width; i++)
        {
            final int finalI = i;
            for (int j = 0; j < height; j++)
                getColor(camera.getCoordinate(finalI, j), height - j - 1, width - 1 - finalI);
//            trGroup.add(new Thread()
//            {
//                @Override
//                public void run()
//                {
//                    for (int j = 0; j < height; j++)
//                        getColor(camera.getCoordinate(finalI, j), height - j - 1, width - 1 - finalI);
                    repaint();
//                }
//            });
//            trGroup.get(i).start();
        }

        try
        {
            for(Thread tr : trGroup)
                tr.join();
        } catch (InterruptedException e)
        {
                e.printStackTrace();
        }
//            while (tr.isAlive())


        DEPTH_BUFFER_ON = true;

        System.out.println("Time rendering: "+(System.currentTimeMillis() - time)+" ms");
    }

    private void getColor(Vector dir, int x, int y)
    {
        double size = model.size;
        Vector a = model.point.clone();
        Vector b = a.clone();
        b.add(size, size, size);
        Vector c = new Vector();
        if(isContains(dir, pos, new Vector[]{a, b}, c))
        {
            double d = c.z;

            c.z = d * (dir.z - pos.z) + pos.z;
            c.y = d * (dir.y - pos.y) + pos.y;
            c.x = d * (dir.x - pos.x) + pos.x;

            Vector delta = dir.multiply(size);
//            Vector delta = dir.multiply(buffer.size * Math.pow(2, model.getLevel() + 1));

            Vector f = c.subtract(a).multiply(size * 2);

            for(int i = 1, n = 2; i < model.getLevel(); i++, n <<= 1)
            {
                delta.divide(2., 2., 2.);

                while(clampVector(f, -1, n) &&
                buffer.childBuffer[getIndex(f, i, n)] == 0)
                {
//                    intersection.add(f.divide(new Vector(size * n)).add(a));
                    f.add(delta.x, delta.y, delta.z);
                }

                f.multiply(2, 2, 2);
            }


            while(f.x > -1 && f.x + 1 < buffer.n &&
                  f.y > -1 && f.y + 1 < buffer.n &&
                  f.z > -1 && f.z + 1 < buffer.n &&
                  buffer.colorBuffer[colorIndex((int) f.x, (int) f.y, (int) f.z)] == 0)
                f.add(delta.x, delta.y, delta.z);

            if(f.x < 0 || f.x + 2 > buffer.n
            || f.y < 0 || f.y + 2 > buffer.n
            || f.z < 0 || f.z + 2 > buffer.n ||
            buffer.colorBuffer[colorIndex((int) f.x, (int) f.y, (int) f.z)] == 0)
            {
                depth_buffer[x][y] = Float.NaN;
                pixels[(x * width + y) * 3    ] = 0;
                pixels[(x * width + y) * 3 + 1] = 0;
                pixels[(x * width + y) * 3 + 2] = 0;
                return;
            }


            int x1 = ((int) f.x), y1 = ((int) f.y), z1 = ((int) f.z);

            int color = buffer.colorBuffer[colorIndex(x1, y1, z1)];
            byte A, R, G, B;
            R = (byte) (color >> 24 & 0xFF);
            G = (byte) (color >> 16 & 0xFF);
            B = (byte) (color >> 8  & 0xFF);
            A = (byte) (color       & 0xFF);

            pixels[(x * width + y) * 3    ] = R;
            pixels[(x * width + y) * 3 + 1] = G;
            pixels[(x * width + y) * 3 + 2] = B;
//
//            double dd = computeNormal(f, buffer.size, buffer.n, buffer.getLevel() - 1).x;
//            dd = 1;

            Vector lNormal = null;

            if(x1 > 0 && y1 > 0 && z1 > 0)
            {
                int r = 1;
                int xP = buffer.colorBuffer[colorIndex(x1 + r, y1, z1)] & 0xFF;
                int xN = buffer.colorBuffer[colorIndex(x1 - r, y1, z1)] & 0xFF;

                int yP = buffer.colorBuffer[colorIndex(x1, y1 + r, z1)] & 0xFF;
                int yN = buffer.colorBuffer[colorIndex(x1, y1 - r, z1)] & 0xFF;

                int zP = buffer.colorBuffer[colorIndex(x1, y1, z1 + r)] & 0xFF;
                int zN = buffer.colorBuffer[colorIndex(x1, y1, z1 - r)] & 0xFF;

                lNormal = new Vector(xN - xP, yN - yP, zN - zP);
                lNormal.x = Math.abs(lNormal.x);
                lNormal.y = Math.abs(lNormal.y);
                lNormal.z = Math.abs(lNormal.z);

                lNormal = lNormal.normalize();
//                System.out.println(lNormal);


            }
//            else return;
            
//            double dd = lNormal.x;
//            pixels[(x * width + y) * 3    ] = (int) (255 * dd);
//            pixels[(x * width + y) * 3 + 2] = (int) (255 * dd);
//            pixels[(x * width + y) * 3 + 1] = (int) (255 * dd);
        }
        else
        {
            pixels[(x * width + y) * 3    ] = 0;
            pixels[(x * width + y) * 3 + 1] = 0;
            pixels[(x * width + y) * 3 + 2] = 0;
        }
    }

    private int colorIndex(int x1, int y1, int z1)
    {
        return (x1 * buffer.n + y1) * buffer.n + z1;
    }

    private boolean isContains(Vector dir, Vector origin, Vector[] bounds, Vector rec)
	{
        double min, max, min1, max1;
        byte sign[] = new byte[3];

        sign[0] = (byte) ((dir.x < 0) ? 1 : 0);
        sign[1] = (byte) ((dir.y < 0) ? 1 : 0);
        sign[2] = (byte) ((dir.z < 0) ? 1 : 0);

        min =  (bounds[sign[0]].x   - origin.x) / dir.x;
        max =  (bounds[1-sign[0]].x - origin.x) / dir.x;
        min1 = (bounds[sign[1]].y   - origin.y) / dir.y;
        max1 = (bounds[1-sign[1]].y - origin.y) / dir.y;

        if ( (min > max1) || (min1 > max) )
            return false;

        if (min1 > min)
            min = min1;

        if (max1 < max)
            max = max1;

        min1 = (bounds[sign[2]].z   - origin.z) / dir.z;
        max1 = (bounds[1-sign[2]].z - origin.z) / dir.z;

        if ( (min > max1) || (min1 > max) )
            return false;

        if (min1 > min)
            min = min1;

        if (max1 < max)
            max = max1;

        double EPS = 1.000000000001;
        rec.z = min * EPS;

        return  (min < max) && (max > 0.f);
	}

    @Override
    public void paint(Graphics g)
    {
//        Graphics2D big = bufferedImage.createGraphics();

//        big.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//        big.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT);

//        doubleBuffer(big);

        bufferedImage.getRaster().setPixels(0, 0, width, height, pixels);

        g.drawImage(bufferedImage, 2, 2, this);
    }

//    private void doubleBuffer(Graphics2D g)
//    {
//        g.setColor(Color.BLACK);
//        g.fillRect(0, 0, width, height);
//        if(DEPTH_BUFFER_ON)
//        {
//            for(int i = 0; i < width; i++)
//                for (int j = 0; j < height; j++)
//                {
////                        g.setColor(new Color((int)(depth_buffer[i][j] * (color_buffer[i][j][0] & 0xFF) ) ,
////                                             (int)(depth_buffer[i][j] * (color_buffer[i][j][1] & 0xFF) ) ,
////                                             (int)(depth_buffer[i][j] * (color_buffer[i][j][2] & 0xFF) ) ));
//                        g.setColor(new Color(color_buffer[i][j][0] & 0xFF,
//                                color_buffer[i][j][1] & 0xFF,
//                                color_buffer[i][j][2] & 0xFF));
////                        g.setColor(new Color(depth_buffer[i][j], depth_buffer[i][j], depth_buffer[i][j]));
////                        g.setColor(new Color(normal_buffer[i][j], normal_buffer[i][j], normal_buffer[i][j]));
//
//                    g.fillRect((width - i), (height - j), 1, 1);
//                }
//        }
//        else
//            for(int i = 0; i < width; i++)
//                for (int j = 0; j < height; j++)
//                {
//                    g.setColor(new Color(color_buffer[i][j][0]&0xFF, color_buffer[i][j][1]&0xFF, color_buffer[i][j][2]&0xFF));
////                    g.setColor(new Color(normal_buffer[i][j], normal_buffer[i][j], normal_buffer[i][j]));
//                    g.fillRect((width - i), (height - j), 1, 1);
//                }
//    }

}