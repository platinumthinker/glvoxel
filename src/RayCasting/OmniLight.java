package RayCasting;

import Mesh.Vector;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 03.11.12
 * Time: 16:16
 */
public class OmniLight
{
    Color color;
    Vector point;
    double size = 0.01;

    public void draw(GL2 gl, GLUT glut)
    {
        gl.glPushMatrix();
        gl.glTranslated(point.x + size, point.y + size, point.z + size);
        gl.glColor3d(color.getRed() / 255., color.getGreen() / 255., color.getBlue() / 255.);
        glut.glutSolidSphere(size*2, 5, 5);
        gl.glPopMatrix();
    }
}
