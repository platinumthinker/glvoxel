package RayCasting;

import Mesh.Vector;
import Voxels.VoxelBuffer;
import Voxels.VoxelModel;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 17.10.12
 * Time: 22:24
 */

public class RenderCPU extends JFrame
{
	Camera camera;
	Vector pos;
	int height, width;
	VoxelModel model;
	byte color_buffer[][][];
    float depth_buffer[][];
    float normal_buffer[][];
    VoxelBuffer buffer;
    OmniLight light;
    ArrayList<Vector> itersection;
    TreeMap<Byte, byte[]> colMap;

    private BufferedImage bufferedImage;
    boolean DEPTH_BUFFER_ON = false;

    public RenderCPU(VoxelModel model)
	{
        itersection = new ArrayList<>();
		this.camera = new RayCasting.Camera(500, 500, new Vector(0, 0, 0));

        pos = camera.pos.clone();
		height = camera.height;
		width = camera.width;
		this.model = model;

        bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		color_buffer = new byte[width][height][4];
        depth_buffer = new float[width][height];
        normal_buffer = new float[width][height];

        light = new OmniLight();
        light.point = new Vector(0,1,1);
        light.color = new Color(255, 253, 244);

		buffer = new VoxelBuffer(model);
        System.out.println("Finish buffering...");

        setSize(width, height);
        setLocation(700, 0);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        colMap = new TreeMap<>();
        byte C = 0;

        for(int i = -128; i < 128; i++)
            colMap.put((byte)i, new byte[]{C, C, C++, -1});

        System.out.println("Start rendering....");
        calculate();
	}

    public void draw(GL2 gl)
	{
//		gl.glPointSize(0.3f);
//		gl.glBegin(GL2.GL_POINTS);
//		for(int i = 0; i < width; i++)
//			for (int j = 0; j < height; j++)
//			{
//				Vector v = camera.getCoordinate(i, j);
//				gl.glColor3ubv(color[i][j], 0);
//				gl.glVertex3d(v.x, v.y, v.z);
//			}
//		gl.glEnd();

//        gl.glColor3d(1, 0, 0);
//        gl.glBegin(GL2.GL_POINTS);
//        for(Vector v : intersection)
//            gl.glVertex3d(v.x, v.y, v.z);
//        gl.glEnd();
        camera.draw(gl);
        light.draw(gl, new GLUT());
	}

	private void calculate()
	{
        long time = System.currentTimeMillis();

        final float[] min = {Float.MAX_VALUE}; final float[] max = {-Float.MAX_VALUE};
        ArrayList<Thread> trGroup = new ArrayList<>();

        for(int i = 0; i < width; i++)
        {
            final int finalI = i;
            trGroup.add(new Thread()
            {
                @Override
                public void run()
                {
                    for (int j = 0; j < height; j++)
                    {
                        Vector a = camera.getCoordinate(finalI, j);
//                        byte[] sign = new byte[3];
//                        sign[0] = (byte) ((inv_dir.x < 0) ? 1 : 0);
//                        sign[1] = (byte) ((inv_dir.y < 0) ? 1 : 0);
//                        sign[2] = (byte) ((inv_dir.z < 0) ? 1 : 0);
//                        getColor1(dir, inv_dir, sign, finalI, j);
                        getColor1(a, finalI, j);

//                        if (!Float.isNaN(depth_buffer[finalI][j]))
//                        {
//                            max[0] = Math.max(depth_buffer[finalI][j], max[0]);
//                            min[0] = Math.min(depth_buffer[finalI][j], min[0]);
//                        }
//                        repaint();
                    }
                }
            });

            trGroup.get(i).start();
        }

        for(Thread tr : trGroup)
            while (tr.isAlive())
                Thread.yield();
        repaint();
//        float min1 = Float.MAX_VALUE, max1 = - Float.MAX_VALUE;
//        for(int i = 0; i < width; i++)
//            for (int j = 0; j < height; j++)
//            {
//                if(!Float.isNaN(normal_buffer[i][j]))
//                {
//                    max1 = Math.max(normal_buffer[i][j], max1);
////                    min1 = Math.min(normal_buffer[i][j], min1);
//                }
//
//                if(!Float.isNaN(depth_buffer[i][j]))
//                    depth_buffer[i][j] = 1 - (depth_buffer[i][j] - min[0]) / (max[0] - min[0]);
//            }


//        DEPTH_BUFFER_ON = true;

        System.out.println("Time rendering: "+(System.currentTimeMillis() - time)+" ms");
    }

    private void getColor1(Vector dir, int x, int y)
    {
        double size = model.size;
        Vector a = model.point.clone();
        Vector b = a.clone();
        b.add(size, size, size);
        Vector c = new Vector();
        if(isContains(dir.invert(), pos, new Vector[]{a, b}, c))
        {
            double d = c.z;

            c.z = d * (dir.z - pos.z) + pos.z;
            c.y = d * (dir.y - pos.y) + pos.y;
            c.x = d * (dir.x - pos.x) + pos.x;

            Vector delta = dir.multiply(0.5 / model.size);
            Vector f = c.subtract(buffer.point).multiply(model.size * 2);
            for(int i = 1, n = 2; i < model.getLevel(); i++, n <<= 1)
            {
                delta = delta.multiply(0.5);

                while(f.x > -1 && f.x + 1 < n &&
                      f.y > -1 && f.y + 1 < n &&
                      f.z > -1 && f.z + 1 < n &&
                      buffer.childBuffer[i][((int) f.x)][((int) f.y)][((int) f.z)] == 0)
                        f.add(delta.x, delta.y, delta.z);

                f.multiply(2, 2, 2);
            }


            while(f.x > -1 && f.x + 1 < buffer.n &&
                  f.y > -1 && f.y + 1 < buffer.n &&
                  f.z > -1 && f.z + 1 < buffer.n &&
                  buffer.colorBuffer[((int) f.x)][((int) f.y)][((int) f.z)] == 0)
                f.add(delta.x, delta.y, delta.z);

            if(f.x < 0 || f.x + 2 > buffer.n
            || f.y < 0 || f.y + 2 > buffer.n
            || f.z < 0 || f.z + 2 > buffer.n ||
            buffer.colorBuffer[((int) f.x)][((int) f.y)][((int) f.z)] == 0)
            {
                depth_buffer[x][y] = Float.NaN;
                color_buffer[x][y] = new byte[]{0,0,0,0};
                return;
            }

//            intersection.add(f.multiply(buffer.size).add(buffer.point));
            int x1 = ((int) f.x), y1 = ((int) f.y), z1 = ((int) f.z);

            //обратное преобразование
//            depth_buffer[x][y] = (float) (f.multiply(buffer.size).add(buffer.point).magnitude() +  buffer.size*(buffer.colorBuffer[x1][y1][z1] & 0xff) / 255.);

            //прозрачность
//            System.out.print((buffer.colorBuffer[x1][y1][z1] & 0xff));
//            System.out.print(' ');
//            System.out.println((buffer.colorBuffer[x1][y1][z1] >> 16 & 0xff));

            int color = buffer.colorBuffer[x1][y1][z1];
            byte A, R, G, B;
            R = (byte) (color >> 24 & 0xFF);
            G = (byte) (color >> 16 & 0xFF);
            B = (byte) (color >> 8  & 0xFF);
            A = (byte) (color       & 0xFF);
//            int x2 = (int) (f.x / 2),
//                y2 = (int) (f.y / 2),
//                z2 = (int) (f.z / 2);
//            byte ff = buffer.childBuffer[model.getLevel() - 1][x2][y2][z2];
////            color_buffer[x][y] = colMap.get(ff);
            color_buffer[x][y] = new byte[]{R, G, B, A};

            if(f.x >= 1 && f.x + 1 <= buffer.n
              && f.y >= 1 && f.y + 1 <= buffer.n
              && f.z >= 1 && f.z + 1 <= buffer.n)
            {
                Vector normal = new Vector(
                        (getAlpha(x1, y1, z1) + getAlpha(x1 + 1, y1, z1) + getAlpha(x1 - 1, y1, z1)) / (765. ),   //255 * 3
                        (getAlpha(x1, y1, z1) + getAlpha(x1, y1 + 1, z1) + getAlpha(x1, y1 - 1, z1)) / (765. ),
                        (getAlpha(x1, y1, z1) + getAlpha(x1, y1, z1 + 1) + getAlpha(x1, y1, z1 - 1)) / (765. )
                );
                normal_buffer[x][y] = 1 - (float) normal.magnitude();
//                normal_buffer[x][y] = (float) (getAlpha(x1, y1, z1) / 255.);
            }
            else
                normal_buffer[x][y] = 1;

//            {
//                byte A, R, G, B;
//                int color = buffer.colorBuffer[x][y][z];
//
//                A = (byte) (color >> 24 & 0xFF);
//                R = (byte) (color >> 16 & 0xFF);
//                G = (byte) (color >> 8  & 0xFF);
//                B = (byte) (color       & 0xFF);
//
//                return new byte[]{R, G, B, A};
//            }
//
//            int A = 0, R = 0, G = 0, B = 0, n1 = 0;
//
//            for (int i = x - 1; i < x + 2; i++)
//                for (int j = y - 1; j < y + 2; j++)
//                    for(int k = z - 1; k < z + 2; k++)
//                    {
//                        int color = buffer.colorBuffer[i][j][k];
//                        int A1 = (color >> 24) & 0xFF;
//                        A += A1;
//                        if(A1 == 0)
//                            continue;
//                        n1++;
//                        R += (color >> 16) & 0xFF;
//                        G += (color >> 8 ) & 0xFF;
//                        B += (color      ) & 0xFF;
//                    }
//
//            return new byte[]{(byte) ((R / n1) & 0xFF),
//                              (byte) ((G / n1) & 0xFF),
//                              (byte) ((B / n1) & 0xFF),
//                              (byte) ((A / n1) & 0xFF)};
        }
        else
        {
//            depth_buffer[x][y] = Float.NaN;
            color_buffer[x][y] = new byte[]{0, 0, 0, 0};
        }
    }

    private double getAlpha(int x, int y, int z)
    {
        return (buffer.colorBuffer[x][y][z] & 0xFF);
    }

    private int middleColor(int color1, int color2)
    {
        byte R = (byte) ( (color1 & 0xFF)       >> 1 );
        byte G = (byte) ( (color1 >> 8  & 0xFF) >> 1 );
        byte B = (byte) ( (color1 >> 16 & 0xFF) >> 1 );
        byte A = (byte) ( (color1 >> 24 & 0xFF) >> 1 );

        R += (byte) ( (color2 & 0xFF)       );
        G += (byte) ( (color2 >> 8  & 0xFF) );
        B += (byte) ( (color2 >> 16 & 0xFF) );
        A += (byte) ( (color2 >> 24 & 0xFF) );

        return ByteBuffer.wrap(new byte[]{R, G, B, A}).getInt();
    }

    private double EPS = 1.000000000001;

	private boolean isContains(Vector inv_dir, Vector origin, Vector[] bounds, Vector rec)
	{
        double min, max, min1, max1;
        byte sign[] = new byte[3];
        sign[0] = (byte) ((inv_dir.x < 0) ? 1 : 0);
        sign[1] = (byte) ((inv_dir.y < 0) ? 1 : 0);
        sign[2] = (byte) ((inv_dir.z < 0) ? 1 : 0);

        min =  (bounds[sign[0]].x   - origin.x) * inv_dir.x;
        max =  (bounds[1-sign[0]].x - origin.x) * inv_dir.x;
        min1 = (bounds[sign[1]].y   - origin.y) * inv_dir.y;
        max1 = (bounds[1-sign[1]].y - origin.y) * inv_dir.y;

        if ( (min > max1) || (min1 > max) )
            return false;

        if (min1 > min)
            min = min1;

        if (max1 < max)
            max = max1;

        min1 = (bounds[sign[2]].z   - origin.z) * inv_dir.z;
        max1 = (bounds[1-sign[2]].z - origin.z) * inv_dir.z;

        if ( (min > max1) || (min1 > max) )
            return false;

        if (min1 > min)
            min = min1;

        if (max1 < max)
            max = max1;

        rec.z = min * EPS;

        return  (min < max) && (max > 0.f);
	}

    @Override
    public void paint(Graphics g)
    {
        Graphics2D big = bufferedImage.createGraphics();

        big.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        big.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT);

        doubleBuffer(big);

        g.drawImage(bufferedImage, 2, 2, this);
    }

    private void doubleBuffer(Graphics2D g)
    {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);
        if(DEPTH_BUFFER_ON)
        {
            for(int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    if(Float.isNaN(normal_buffer[i][j]))
                        continue;
                    else
//                        g.setColor(new Color((int)(depth_buffer[i][j] * (color_buffer[i][j][0] & 0xFF) ) ,
//                                             (int)(depth_buffer[i][j] * (color_buffer[i][j][1] & 0xFF) ) ,
//                                             (int)(depth_buffer[i][j] * (color_buffer[i][j][2] & 0xFF) ) ));
                        g.setColor(new Color(color_buffer[i][j][0] & 0xFF,
                                             color_buffer[i][j][1] & 0xFF,
                                             color_buffer[i][j][2] & 0xFF ));
//                        g.setColor(new Color(depth_buffer[i][j], depth_buffer[i][j], depth_buffer[i][j]));
//                        g.setColor(new Color(normal_buffer[i][j], normal_buffer[i][j], normal_buffer[i][j]));
//                            g.setColor(new Color((int)(Math.abs(camera.mas[i][j].x) * 255),
//                                (int)(Math.abs(camera.mas[i][j].y) * 255),
//                                (int)(Math.abs(camera.mas[i][j].z) * 255)));
                    g.fillRect(width - i, height - j, 1, 1);
                }
        }
        else
            for(int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    g.setColor(new Color(color_buffer[i][j][0]&0xFF, color_buffer[i][j][1]&0xFF, color_buffer[i][j][2]&0xFF));
//                    g.setColor(new Color(normal_buffer[i][j], normal_buffer[i][j], normal_buffer[i][j]));
//                        g.setColor(new Color((int)(Math.abs(camera.mas[i][j].x) * 255),
//                                (int)(Math.abs(camera.mas[i][j].y) * 255),
//                                (int)(Math.abs(camera.mas[i][j].z) * 255)));
                    g.fillRect(width - i,height - j,1,1);
                }
    }

    @Deprecated
    private Color getColor(Vector dir, Vector inv_dir, byte[] sign)
    {
        double size = model.size;
        Vector a = model.point.clone();
        Vector b = a.clone();
        b.add(size, size, size);
        Vector c = new Vector();
        if(isContains(inv_dir, pos, new Vector[]{a, b}, c))
        {
            double d = c.z;
            c.z = d * (dir.z - pos.z) + pos.z;
            c.y = d * (dir.y - pos.y) + pos.y;
            c.x = d * (dir.x - pos.x) + pos.x;

            Vector delta = dir.multiply(buffer.size);
            Vector f = c.subtract(buffer.point).multiply(1./buffer.size);

            while(f.x > -1 && f.x + 1 < buffer.n &&
                    f.y > -1 && f.y + 1 < buffer.n &&
                    f.z > -1 && f.z + 1 < buffer.n &&
                    buffer.colorBuffer[((int) f.x)][((int) f.y)][((int) f.z)] == 0)
                f = f.add(delta);


            if(f.x <= 0 || f.x + 1>= buffer.n
                    || f.y < 0 || f.y + 1>= buffer.n
                    || f.z < 0 || f.z + 1>= buffer.n)
                return Color.BLACK;

            return new Color(buffer.colorBuffer[((int) f.x)][((int) f.y)][((int) f.z)]);
        }
        else
            return Color.BLACK;
    }
}