package RayCasting;

import Mesh.Vector;

import javax.media.opengl.GL2;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

import static com.jogamp.common.nio.Buffers.newDirectFloatBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 17.10.12
 * Time: 22:24
 */

public class Camera
{
    int height, width;
    double degrees, aspect;
    public Vector pos, look1, look2, look3, look4;
    Vector[][] mas;
    private DoubleBuffer buffer;

    public Camera(int height, int width, Vector pos)
    {
        this.height = height;
        this.width = width;
        this.degrees = 45;
        this.pos = pos;

        look1 = new Vector();
        look1.y -= height/2.;
        look1.z = height/(2. * Math.tan(Math.toRadians(degrees)));
        look1.x = width/(2. * Math.tan(Math.toRadians(degrees)));
        look1 = look1.normalize();
        look1.multiply(0.2 ,0.2 ,0.2);

        look2 = new Vector();
        look2.y -= height/2.;
        look2.z = height/(2. * Math.tan(Math.toRadians(degrees)));
        look2.x -= width/(2. * Math.tan(Math.toRadians(degrees)));
        look2 = look2.normalize();
        look2.multiply(0.2 ,0.2 ,0.2);

        look3 = new Vector();
        look3.y = height/2.;
        look3.z = height/(2. * Math.tan(Math.toRadians(degrees)));
        look3.x = width/(2. * Math.tan(Math.toRadians(degrees)));
        look3 = look3.normalize();
        look3.multiply(0.2 ,0.2 ,0.2);

        look4 = new Vector();
        look4.y = height/2.;
        look4.z = height/(2. * Math.tan(Math.toRadians(degrees)));
        look4.x -= width/(2. * Math.tan(Math.toRadians(degrees)));
        look4 = look4.normalize();
        look4.multiply(0.2 ,0.2 ,0.2);

        look1 = look1.add(pos);
        look2 = look2.add(pos);
        look3 = look3.add(pos);
        look4 = look4.add(pos);

        mas = new Vector[width][height];

        double dx = (look1.x - look2.x) / width;
        double dy = (look3.y - look2.y) / height;

        int i = 0, j = 0;
        for (double x = look2.x; x < look1.x; x += dx)
        {
            for (double y = look2.y; y < look3.y; y += dy)
            {
                mas[i][j++] = new Vector(x, y, look4.z).subtract(pos).normalize();
                if(j == height)
                    break;
            }
            i++; j = 0;
            if(i == width)
                break;
        }
    }

	/**
	 * return coordinate the pixel with position (x y)
	 * @return position this coordinate or null if out of bounds
	 */
	public Vector getCoordinate(int x, int y)
	{
		if(x < 0 || y < 0 || x >= width || y >= height)
			return null;
		else
			return mas[x][y];
	}

	public void draw(GL2 gl)
	{
//        gl.glPointSize(1);
//        gl.glBegin(GL2.GL_POINTS);
//        for(Vector[] col : mas)
//            for (Vector a : col)
//                gl.glVertex3d(a.x/7., a.y/7., a.z/7.);
//        gl.glEnd();

		gl.glPointSize(4);
		gl.glBegin(GL2.GL_POINTS);
		    gl.glVertex3d(pos.x, pos.y, pos.z);
		gl.glEnd();

		gl.glColor3d(1,1,1);
		gl.glBegin(GL2.GL_LINES);
            gl.glVertex3d(look1.x, look1.y, look1.z);
            gl.glVertex3d(look2.x, look2.y, look2.z);
            gl.glVertex3d(look3.x, look3.y, look3.z);
            gl.glVertex3d(look4.x, look4.y, look4.z);

            gl.glVertex3d(look1.x, look1.y, look1.z);
            gl.glVertex3d(look3.x, look3.y, look3.z);
            gl.glVertex3d(look2.x, look2.y, look2.z);
            gl.glVertex3d(look4.x, look4.y, look4.z);

            gl.glVertex3d(pos.x, pos.y, pos.z);
            gl.glVertex3d(look1.x, look1.y, look1.z);
            gl.glVertex3d(pos.x, pos.y, pos.z);
            gl.glVertex3d(look2.x, look2.y, look2.z);
            gl.glVertex3d(pos.x, pos.y, pos.z);
            gl.glVertex3d(look3.x, look3.y, look3.z);
            gl.glVertex3d(pos.x, pos.y, pos.z);
            gl.glVertex3d(look4.x, look4.y, look4.z);
		gl.glEnd();
	}

    public FloatBuffer getBuffer()
    {
        float [] buf = new float[height * width * 3];

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
            {
                buf[i * height + j + 0] = (float) mas[i][j].x;
                buf[i * height + j + 1] = (float) mas[i][j].y;
                buf[i * height + j + 2] = (float) mas[i][j].z;
            }

        return newDirectFloatBuffer(buf);
    }
}
