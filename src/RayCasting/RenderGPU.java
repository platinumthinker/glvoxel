package RayCasting;

import Mesh.Vector;
import Voxels.*;
import com.jogamp.opencl.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;

import static com.jogamp.opencl.CLProgram.define;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 18.11.12
 * Time: 3:42
 */

public class RenderGPU extends JFrame
{
    Camera camera;
    Vector camera_point;
    int height, width;

    VoxelModel model;
    VoxelBufferFlat buffer;
    OmniLight light;

    private boolean isRunning = true;
    CLCommandQueue queue;
    CLContext context;


    private int[] pixels;
    private BufferedImage frameImage;
    CLEventList list;
    private int elementCount;
    int x = -10, y = -10;
    private CLKernel kernel;

    private CLBuffer<IntBuffer>    image;
    private CLBuffer<DoubleBuffer> depth_buffer;
    private CLBuffer<DoubleBuffer> rays_buffer;
    private CLBuffer<ByteBuffer>   child_buffer;
    private CLBuffer<IntBuffer>    voxel_buffer;
    private CLBuffer<DoubleBuffer> camera_pos;
    private CLBuffer<DoubleBuffer> model_pos;
    private CLBuffer<DoubleBuffer> buffer_pos;
    private CLProgram program;


    public RenderGPU(VoxelModel model)
    {
        this.camera = new Camera(500, 500, new Vector(0, 0, 0));

        camera_point = camera.pos.clone();
        height = camera.height;
        width = camera.width;
        this.model = model;

        light = new OmniLight();
        light.point = new Vector(0,1,1);
        light.color = new Color(255, 253, 244);

        buffer = new VoxelBufferFlat(model);
        System.out.println("Finish buffering...");

        setSize(width, height);
        setLocation(700, 0);
        setCursor(getToolkit().createCustomCursor(new BufferedImage( 1, 1, BufferedImage.TYPE_INT_ARGB ), new Point(), null));
        setVisible(true);

        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                isRunning = false;
                context.release();
                System.exit(0);
            }
        });

        addMouseMotionListener(new MouseMotionListener()
        {
            @Override
            public void mouseDragged(MouseEvent e)
            {
                x = e.getX();
                y = e.getY();
            }

            @Override
            public void mouseMoved(MouseEvent e)
            {
                x = e.getX();
                y = e.getY();
            }
        });

        frameImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        add(new JLabel(new ImageIcon(frameImage)));
        setSize(500, 500);
        pack();

        initCL();



        new Runnable()
        {
            @Override
            public void run()
            {
                while (isRunning)
                {
                    long time = System.currentTimeMillis();
                    renderFrame();
                    repaint();
//                    System.out.println((System.currentTimeMillis() - time)); //5ms - minimum delay
                    sleep(20 - (System.currentTimeMillis() - time));
                    setTitle(String.format("FPS %f", 1000./(System.currentTimeMillis() - time)));
                }
            }
        }.run();
    }

    private void sleep(long time)
    {
        if(time < 1)
            return;

        try
        {
            Thread.sleep(time);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void stop()
    {
        isRunning = false;
    }

    private void renderFrame()
    {
        image.getBuffer().rewind();

        queue.put1DRangeKernel(kernel, 0, elementCount, 25);
        queue.putReadBuffer(image, false);

        image.getBuffer().get(pixels);
        frameImage.getRaster().setPixels(0, 0, width, height, pixels);

        if(!camera_pos.isReleased())
            camera_pos.release();
        camera.pos.y += 0.02;

//        camera position
        camera_pos = context.createDoubleBuffer(3, CLMemory.Mem.READ_WRITE, CLMemory.Mem.USE_BUFFER);
        camera_pos.getBuffer().
                put(camera.pos.x).
                put(camera.pos.y).
                put(camera.pos.z).
                rewind();

        queue.finish();
//        if(x > 0 && y > 0 && x < width - 1 && y < height - 1)
//        {
//            pixels[(x + y * width) * 3]           = 255;
//            pixels[(x + y * width) * 3 + 1]       = 255;
//            pixels[(x + y * width) * 3 + 2]       = 255;
//            pixels[(x + y * width + 1) * 3]       = 255;
//            pixels[(x + y * width + 1) * 3 + 1]   = 255;
//            pixels[(x + y * width + 1) * 3 + 2]   = 255;
//            pixels[(x + y * width - 1) * 3]       = 255;
//            pixels[(x + y * width - 1) * 3 + 1]   = 255;
//            pixels[(x + y * width - 1) * 3 + 2]   = 255;
//            pixels[(x + (y + 1) * width) * 3]     = 255;
//            pixels[(x + (y + 1) * width) * 3 + 1] = 255;
//            pixels[(x + (y + 1) * width) * 3 + 2] = 255;
//            pixels[(x + (y - 1) * width) * 3]     = 255;
//            pixels[(x + (y - 1) * width) * 3 + 1] = 255;
//            pixels[(x + (y - 1) * width) * 3 + 2] = 255;
//        }

    }

    public CLDevice getCompatibleDevice()
    {

        CLPlatform[] platforms = CLPlatform.listCLPlatforms();
        for (CLPlatform platform : platforms)
        {
            CLDevice[] devices = platform.listCLDevices();

            for (CLDevice device : devices)
            {
                if(device.isImageSupportAvailable())
                {
                    return device;
                }
            }
        }

        return null;
    }

    private void initCL()
    {
        elementCount = width * height;

        CLDevice device = getCompatibleDevice();
        context = CLContext.create(device);

        try{
            System.out.println("using " + device);

            queue = device.createCommandQueue();

//            int localWorkSize = min(device.getMaxWorkGroupSize(), 256);  // Local work size dimensions
//            int globalWorkSize = roundUp(localWorkSize, elementCount);   // rounded up to the nearest multiple of the localWorkSize

            pixels = new int[elementCount * 3];

            program = context.createProgram(ClassLoader.getSystemResourceAsStream("RayCasting/color.cl")).
                    build(define("HEIGHT", height),
                          define("WIDTH", width),
                          define("SIZE", model.size),
                          define("EPS", 1.000000000001),
                          define("LEVEL", model.getLevel()),
                          define("N", buffer.n));

            //color_buffer
            image = context.createIntBuffer(elementCount * 3, CLMemory.Mem.WRITE_ONLY);
            image.getBuffer().rewind();
            //depth buffer double*
            depth_buffer = context.createDoubleBuffer(elementCount * 3, CLMemory.Mem.WRITE_ONLY);

            //rays buffer double3*
            rays_buffer = context.createDoubleBuffer(height * width * 3, CLMemory.Mem.READ_WRITE, CLMemory.Mem.USE_BUFFER);
            for (int i = 1; i <= width; i++)
                for (int j = 1; j <= height; j++)
                {
                    Vector a = camera.mas[height - j][width - i];
                    rays_buffer.getBuffer().put(a.x).put(a.y).put(a.z);
                }
            rays_buffer.getBuffer().rewind();

            //child buffer byte*
            child_buffer = context.createByteBuffer(buffer.childBuffer.length, CLMemory.Mem.READ_WRITE, CLMemory.Mem.USE_BUFFER);
            child_buffer.getBuffer().put(buffer.childBuffer).rewind();

            //color voxel buffer int*
            voxel_buffer = context.createIntBuffer(buffer.colorBuffer.length, CLMemory.Mem.READ_WRITE, CLMemory.Mem.USE_BUFFER);
            voxel_buffer.getBuffer().put(buffer.colorBuffer).rewind();

            //camera position
            camera_pos = context.createDoubleBuffer(3, CLMemory.Mem.READ_WRITE, CLMemory.Mem.USE_BUFFER);
            camera_pos.getBuffer().
                                   put(camera.pos.x).
                                   put(camera.pos.y).
                                   put(camera.pos.z).
                                   rewind();

            //model position
            model_pos = context.createDoubleBuffer(3, CLMemory.Mem.READ_WRITE, CLMemory.Mem.USE_BUFFER);
            model_pos.getBuffer().
                                  put(model.point.x).
                                  put(model.point.y).
                                  put(model.point.z).
                                  rewind();

//            queue.putWriteBuffer(rays_buffer,  false);
//            queue.putWriteBuffer(child_buffer, false);
//            queue.putWriteBuffer(voxel_buffer, false);
//            queue.putWriteBuffer(camera_pos,   false);
//            queue.putWriteBuffer(model_pos,    false);
//            queue.putWriteBuffer(buffer_pos,   false);

            kernel = program.createCLKernel("color").
                    putArgs(image,
                            depth_buffer,
                            rays_buffer,
                            child_buffer,
                            voxel_buffer,
                            camera_pos,
                            model_pos).
                            rewind();

            System.out.println(program.getBuildStatus(device));
            System.out.println(program.getBuildLog());


            CLUserEvent ev = CLUserEvent.create(context);
            ev.registerCallback(new CLEventListener()
            {
                @Override
                public void eventStateChanged(CLEvent clEvent, int i)
                {
                    if(clEvent.getStatus() == CLEvent.ExecutionStatus.COMPLETE)
                    {
                        image.getBuffer().rewind();
                        image.getBuffer().get(pixels);
                        frameImage.getRaster().setPixels(0, 0, width, height, pixels);

                        queue.put1DRangeKernel(kernel, 0, elementCount, 25);
                        queue.putReadBuffer(image, false);
                        queue.finish();
                    }
                }
            });



        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static int roundUp(int groupSize, int globalSize)
    {
        int r = globalSize % groupSize;
        if (r == 0)
        {
            return globalSize;
        } else
        {
            return globalSize + groupSize - r;
        }
    }

}
