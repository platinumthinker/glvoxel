package Voxels;

import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;
import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 20.10.12
 * Time: 1:29
 */

public class VoxelBuffer extends VoxelModel
{
	public byte[][][][] childBuffer;
	public int[][][] colorBuffer;
    ByteBuffer buffer;
    public int n;

    public VoxelBuffer(VoxelModel voxelModel)
	{
        buffer = ByteBuffer.allocate(4);
		fillBuffers(voxelModel);
	}

	public void fillBuffers(VoxelModel model)
	{
		this.level = model.level;
		this.leveldraw = 1;
        this.point = model.point;
        this.size = model.size / (1 << level);
        root = model.getRoot();

		childBuffer = new byte[level][][][];

		n = 1;
		long aa = 0;
        for(int i = 0; i < level; i++, n <<= 1)
        {
            childBuffer[i] = new byte[n][n][n];
        }

        colorBuffer = new int[n][n][n];

        fillLevel(root, 0, 0, 0, 0);
	}

	private void fillLevel(Voxel voxel, int level, int x, int y, int z)
	{
		switch (voxel.getType())
		{
			case GRAY_VOXEL:
							childBuffer[level++][x][y][z] = voxel.getGVoxel().getChildren();
			                GrayVoxelIterator it = voxel.getGVoxel().iterator();

							while (it.hasNext())
							{
								Voxel v = it.next(); int num[] = it.getCoordinate(x, y, z);
								fillLevel(v, level, num[0], num[1], num[2]);
							}
						    break;
			case BLACK_VOXEL:
				if(this.level == level)
				    colorBuffer[x][y][z] = voxel.getBVoxel().getColorInt();
				else
				{
				    int color = voxel.getBVoxel().getColorInt();
					int n = 1 << (this.level - level);
					for(int i = x * n; i < x*n + n; i++)
						for(int j = y * n; j < y*n + n; j++)
							for(int k = z * n; k < z*n + n; k++)
								colorBuffer[i][j][k] = color;
				}
				break;
		}
	}

	@Override
	public type getType()
	{
		return type.VOXEL_BUFFER;
	}

	@Override
	public void draw(GL2 gl, GLUT glu)
	{
        byte c[];
        double size_2 = size/2;
        gl.glPushMatrix();
        gl.glTranslated(point.x + size_2, point.y + size_2, point.z + size_2);

        for(int[][] rect : colorBuffer)
        {
            gl.glPushMatrix();
            for(int[] column : rect)
            {
                gl.glPushMatrix();
                for(int color : column)
                {
                    if(color != 0)
                    {
                        c = buffer.putInt(color).array();
                        buffer = (ByteBuffer) buffer.clear();
//                        gl.glColor4ub(c[0], c[1], c[2], c[3]);
                        gl.glColor3ub(c[0], c[1], c[2]);
                        glu.glutSolidCube((float) size);
                    }
                    gl.glTranslated(0, 0, size);
                }
                gl.glPopMatrix();
                gl.glTranslated(0, size, 0);
            }
            gl.glPopMatrix();
            gl.glTranslated(size, 0, 0);
        }
        gl.glPopMatrix();
    }
}