package Voxels;

import Mesh.Vector;

import java.awt.*;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 17.10.12
 * Time: 0:32
 */

public class FabricVoxelTerrain
{
    Random r;
    private double vertex[][];
    private double vertex_norm[][];
    private Color[][] texture;

    public FabricVoxelTerrain()
    {
        r = new Random();
    }

    public VoxelModel generic(double size, Vector pos, byte level)
    {
        GrayVoxel root = new GrayVoxel();

        int width = (int) Math.pow(2, level + 1);

        genericHeight(width);
        calculateNormal(width);
        genericTexture(width);
        resizingVertex(width);
        Vector a = pos.add(new Vector(width/2., width/2., width/2.));

        genericVoxels(root, level, a, width / 2);

        return new VoxelModel(root, level, size, pos);
    }

    private void genericVoxels(GrayVoxel voxel, byte level, Vector pos, double size)
    {
        if(level-- < 0)
            return;

        size/=2.;
        Vector[] mas = new Vector[8];
        mas[0] = new Vector(pos.x - size, pos.y - size, pos.z - size);
        mas[1] = new Vector(pos.x + size, pos.y - size, pos.z - size);
        mas[2] = new Vector(pos.x - size, pos.y - size, pos.z + size);
        mas[3] = new Vector(pos.x + size, pos.y - size, pos.z + size);
        mas[4] = new Vector(pos.x - size, pos.y + size, pos.z - size);
        mas[5] = new Vector(pos.x + size, pos.y + size, pos.z - size);
        mas[6] = new Vector(pos.x - size, pos.y + size, pos.z + size);
        mas[7] = new Vector(pos.x + size, pos.y + size, pos.z + size);

        switch (level)
        {
            case 0:
                Voxel[] child = new Voxel[8];

                for(int i = 0; i < 8; i++)
                    child[i] = new BlackVoxel(texture[((int) mas[i].x)][((int) mas[i].z)]);

                voxel.setChildren(child, (byte) 0b1111_1111);
                break;
            case 1:
                byte num = 0;
                GrayVoxel[] child1 = new GrayVoxel[8];

                for (int i = 0; i < 8; i++)
                {
                    if(vertex[((int) mas[i].x)][((int)mas[i].z)] <= mas[i].y)
                    {
                        child1[i] = new GrayVoxel();
                        genericVoxels(child1[i], level, mas[i], size);
                        num |= 1 << i;
                    }
                }

                voxel.setChildren(child1, num);
                break;
            default:
                GrayVoxel[] child2 = new GrayVoxel[8];
                for (int i = 0; i < 8; i++)
                {
                    child2[i] = new GrayVoxel();
                    genericVoxels(child2[i], level, mas[i], size);
                }

                voxel.setChildren(child2, (byte) 0b1111_1111);
                break;
        }
    }

    private void genericHeight(int width)
    {
        int N = (int) (Math.floor((Math.log(width)/Math.log(2))));

        int N_2 = (int) Math.pow(2, N+1) + 1;
        int SEED = 7;
        vertex = new double[N_2][N_2];

        vertex[0][0] = vertex[0][N_2-1] =
                vertex[N_2-1][0] = vertex[N_2-1][N_2-1] = SEED;

        int h = 500;

        for(int sideLength = N_2-1; sideLength >= 2; sideLength /=2, h/= 2.0)
        {
            int halfSide = sideLength/2;

            for(int x=0;x<N_2-1;x+=sideLength)
                for(int y=0;y<N_2-1;y+=sideLength)
                {
                    double avg = (vertex[x][y] + vertex[x+sideLength][y] +
                            vertex[x][y+sideLength] + vertex[x+sideLength][y+sideLength]) / 4.0;

                    vertex[x+halfSide][y+halfSide] = (int) ((avg ) + (r.nextDouble()*2*h) - h);
                }

            for(int x=0;x<N_2-1;x+=halfSide)
                for(int y=(x+halfSide)%sideLength;y<N_2-1;y+=sideLength)
                {
                    vertex[x][y]  =  (int) ((vertex[(x-halfSide+N_2)%N_2][y] +
                            vertex[(x+halfSide)%N_2][y] +
                            vertex[x][(y+halfSide)%N_2] +
                            vertex[x][(y-halfSide+N_2)%N_2]) / 4. +(r.nextDouble()*2*h) - h);

                    if(x == 0)  vertex[N_2-1][y] = vertex[x][y];
                    if(y == 0)  vertex[x][N_2-1] = vertex[x][y];
                }
        }

        for (h = 0; h < 1; h ++)
            for (int i = 1 ; i < width-1 ; i++)
                for (int j = 1 ; j < width-1; j++)
                {
                    vertex[i][j] += vertex[i-1][j] + vertex[i+1][j] + vertex[i][j-1] + vertex[i][j+1]
                            + vertex[i-1][j-1] + vertex[i-1][j+1] + vertex[i+1][j+1] + vertex[i+1][j-1];
                    vertex[i][j] /= 9;
                }

        double min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        for (int i = 0 ; i < width ; i ++)
            for (int j = 0 ; j < width; j++)
            {
                if(vertex[i][j] > max)
                    max = vertex[i][j];
                if(vertex[i][j] < min)
                    min = vertex[i][j];
            }

        if(min < 0)
        {
            for (int i = 0 ; i < width ; i ++)
                for (int j = 0 ; j < width; j++)
                    vertex[i][j] -= min;
            max -= min; min = 0;
        }

        double k = max - min;

        for (int i = 0 ; i < width ; i ++)
            for (int j = 0 ; j < width; j++)
            {
                double buf = (vertex[i][j] / max);
                vertex[i][j] = (buf * 255.);
            }
    }

    private void calculateNormal(int width)
    {
        vertex_norm = new double[width][width];
        int NumOfIndexes = (width-1)*(width-1)*6;
        int indices[] = new int[NumOfIndexes];

        int t,V=0;
        for(int i=0; i<width-1; i++)
            for(int j=0; j<width-1; j++)
            {
                t=i*width+j;
                indices[V]   = t;
                indices[V+1] = t+width;
                indices[V+2] = t+width+1;
                indices[V+3] = t;
                indices[V+4] = t+width+1;
                indices[V+5] = t+1;
                V+=6;
            }


        int v1, v2, v3;
        float3 a = new float3(), b = new float3(),
                c = new float3(), p, q, n = new float3();

        for(int i=0; i<NumOfIndexes; i+=3)
        {
            v1 = indices[i];
            v2 = indices[i + 1];
            v3 = indices[i + 2];

            a.y = vertex[v1 / width][v1 % width];
            a.x = v1 / width; a.z = v1 % width;

            b.y =  vertex[v2 / width][v2 % width];
            b.x = v2 / width; b.z = v2 % width;

            c.y =  vertex[v3 / width][v3 % width];
            c.x = v3 / width; c.z = v3 % width;

            p = b.Sub(a);
            q = c.Sub(b);

            n.x = q.y * p.z - q.z * p.y;
            n.y = q.z * p.x - q.x * p.z;
            n.z = q.x * p.y - q.y * p.x;

            double l = Math.sqrt(n.x*n.x + n.y*n.y + n.z*n.z);
            n.x /= l;
            n.y /= l;
            n.z /= l;

            vertex_norm[v1/width][v1%width] =
            vertex_norm[v2/width][v2%width] =
            vertex_norm[v3/width][v3%width] =
                    ((Math.atan2(Math.sqrt(n.x*n.x+n.z*n.z), n.y)*2)/Math.PI);
        }
    }

    private void genericTexture(int width)
    {
        texture = new Color[width][width];
        for (int i = 0 ; i < width ; i ++)
            for (int j = 0 ; j < width ; j ++)
            {
                Color a,b,c,d,e,f;
                a = new Color(159,102, 83,(int)(255*(cov(4, vertex[i][j], vertex_norm[i][j]))));
                b = new Color(119,121, 83,(int)(255*(cov(1, vertex[i][j], vertex_norm[i][j]))));
                c = new Color(186,171, 35,(int)(255*(cov(5, vertex[i][j], vertex_norm[i][j]))));
                d = new Color( 39,129, 37,(int)(255*(cov(3, vertex[i][j], vertex_norm[i][j]))));
                e = new Color(116,236, 28,(int)(255*(cov(2, vertex[i][j], vertex_norm[i][j]))));
                f = new Color(255,255,255,(int)(255*(cov(0, vertex[i][j], vertex_norm[i][j]))));
                texture[i][j] = AlphaBlending(AlphaBlending(AlphaBlending(AlphaBlending(AlphaBlending(a, b), c), d), e), f);
            }

        for (int i = 1 ; i < width-1 ; i++)
            for (int j = 1 ; j < width-1; j++)
            {
                int bufR = texture[i][j].getRed() + texture[i-1][j].getRed() + texture[i+1][j].getRed() + texture[i][j-1].getRed() + texture[i][j+1].getRed()
                        + texture[i-1][j-1].getRed() + texture[i-1][j+1].getRed() + texture[i+1][j+1].getRed() + texture[i+1][j-1].getRed();
                bufR /= 9;
                int bufG = texture[i][j].getGreen() + texture[i-1][j].getGreen() + texture[i+1][j].getGreen() + texture[i][j-1].getGreen() + texture[i][j+1].getGreen()
                        + texture[i-1][j-1].getGreen() + texture[i-1][j+1].getGreen() + texture[i+1][j+1].getGreen() + texture[i+1][j-1].getGreen();
                bufG /= 9;
                int bufB = texture[i][j].getBlue() + texture[i-1][j].getBlue() + texture[i+1][j].getBlue() + texture[i][j-1].getBlue() + texture[i][j+1].getBlue()
                        + texture[i-1][j-1].getBlue() + texture[i-1][j+1].getBlue() + texture[i+1][j+1].getBlue() + texture[i+1][j-1].getBlue();
                bufB /= 9;
                texture[i][j] = new Color(bufR, bufG, bufB);
            }

        for (int i = 0 ; i < width ; i ++)
            for (int j = 0 ; j < width ; j ++)
                if(vertex[i][j] < 70)
                    texture[i][j] = Color.BLUE;

    }

    private Color AlphaBlending(Color back, Color foreg)
    {
        double transparent = foreg.getAlpha()/255.0;
        return new Color((int)(back.getRed()*(1-transparent)+foreg.getRed()*transparent),
                (int)(back.getGreen()*(1-transparent)+foreg.getGreen()*transparent),
                (int)(back.getBlue()*(1-transparent)+foreg.getBlue()*transparent));
    }

    /*определение вносимого данным слоем изменения
    sw - слой
    h - высота точки 0..255
    angle - нормаль точки 0..1    */
    private double cov(int sw, double h, double angle)
    {
        switch(sw)
        {
            case 0: //снег холмы
                return func_raspred(h, 200, 256, 25, 0.01)*
                        func_raspred(angle, 0.6, 1, 0.2, 0.2);

            case 1: //гранит горы
                return func_raspred(h, 180, 256, 25, 0.01)*
                        func_raspred(angle, 0, 0.5, 0.1, 0.2);

            case 2: //светл. трава
                return func_raspred(h, 140, 200, 40, 10)*
                        func_raspred(angle, 0.4, 0.5, 0.1, 0.2);

            case 3: //темн. трава
                return func_raspred(h, 50, 160, 10, 20)*
                        func_raspred(angle, 0.6, 1, 0.2, 0.01);

            case 4: //земля
                return func_raspred(h, 50, 190, 30, 5)*
                        func_raspred(angle, 0, 0.7, 0.1, 0.2);

            case 5: //песок
                return func_raspred(h, 0, 40, 0.01, 20)*
                        func_raspred(angle, 0, 1, 0.01, 0.01);
        }
        return 0;
    }

    private double func_raspred(double x, double min, double max, double deltamin, double deltamax)
    {
        return Math.max(Math.min(Math.min(x/deltamin+(1-min/deltamin),
                - x/deltamax+(1+max/deltamax)), 1), 0);
    }

    private void resizingVertex(double size)
    {
        for (int i = 0 ; i < vertex.length ; i ++)
            for (int j = 0 ; j < vertex.length; j++)
                vertex[i][j] = (size * vertex[i][j] / 255.);
    }

}

class float3
{
    public double x,y,z;
    public float3 Sub(float3 a)
    {
        float3 e = new float3();
        e.x = x - a.x;
        e.y = y - a.y;
        e.z = z - a.z;
        return e;
    }
}