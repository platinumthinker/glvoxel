package Voxels;

import Mesh.Mesh;
import Mesh.Vector;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

import static Voxels.Voxel.type;
import static java.lang.Math.acos;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 04.10.12
 * Time: 16:48
 */

public class TestIntersect
{
    private ArrayList<ArrayList<Integer>> faces;
    private TreeSet<Integer[]> lines;
    private ArrayList<Vector> vertexes, normals, normals1;
    private Vector min;
    double sum = 0, maxi = -100, mini = 100;
    int N = 0;
    long f = 0;
    ArrayList<Vector> quad = new ArrayList<>();
    ArrayList<type> type_quad = new ArrayList<>();

    public TestIntersect(Mesh mesh)
    {
        lines = new TreeSet<>(new Comparator<Integer[]>()
        {
            @Override
            public int compare(Integer[] o1, Integer[] o2)
            {
                int a = o1[0].compareTo(o2[0]);
                return (a == 0) ? o1[1].compareTo(o2[1]) : a;}
        });

        faces = mesh.getFace();
        vertexes = mesh.getVertexs();
        normals1 = new ArrayList<>();
        normals = mesh.getNormals();

        for(ArrayList<Integer> face : faces)
        {
            Vector c = vertexes.get(face.get(0));
            Vector a = vertexes.get(face.get(3)).subtract(c);
            Vector b = vertexes.get(face.get(6)).subtract(c);
            normals1.add(a.cross(b).normalize());

            int size = face.size();
            for(int i = 0; i < size/3; i++)
            {
                int x = face.get(i*3);
                int y = face.get(((i + 1)*3) % size);

                if (x < y)
                    lines.add(new Integer[]{x, y});
                else
                    lines.add(new Integer[]{y, x});
            }
        }

        min = mesh.min.clone();
        min.subtract(0, 0, 10);
        min.y = mesh.center.y - mesh.size / 2;
    }

    public type typeCube(Vector a, double size)
    {
        return typeCube1(a, size);
//        Vector p[] = new Vector[7];
//        p[0] = new Vector(a.x + size, a.y,        a.z);
//        p[1] = new Vector(a.x,        a.y,        a.z + size);
//        p[2] = new Vector(a.x + size, a.y,        a.z + size);
//        p[3] = new Vector(a.x,        a.y + size, a.z);
//        p[4] = new Vector(a.x + size, a.y + size, a.z);
//        p[5] = new Vector(a.x,        a.y + size, a.z + size);
//        p[6] = new Vector(a.x + size, a.y + size, a.z + size);
//
//        Vector[][] l = new Vector[][]{new Vector[]{a,    p[0]},
//                new Vector[]{a,    p[1]},
//                new Vector[]{a,    p[3]},
//                new Vector[]{p[2], p[0]},
//                new Vector[]{p[4], p[0]},
//                new Vector[]{p[1], p[2]},
//                new Vector[]{p[1], p[5]},
//                new Vector[]{p[2], p[6]},
//                new Vector[]{p[3], p[4]},
//                new Vector[]{p[3], p[5]},
//                new Vector[]{p[4], p[6]},
//                new Vector[]{p[5], p[6]}};
//
//        for(int i = 0; i < 12; i++)
//        {
//            Iterator<ArrayList<Integer>> face = faces.iterator();
//            Iterator<Vector> normal = normals.iterator();
//
//            TreeSet<Vector> in = new TreeSet<>();
//            while(face.hasNext())
//                if(inPolygon(face.next(), l[i], in, normal.next()))
//                    return type.GRAY_VOXEL;
//        }
//
//        if(!isSquareIntersect(a, p[0], p[4], p[3]))
//            if(!isSquareIntersect(p[0], p[2], p[6], p[4]))
//                if(!isSquareIntersect(p[2], p[1], p[5], p[6]))
//                    if(!isSquareIntersect(p[1], a, p[3], p[5]))
//                        if(!isSquareIntersect(p[3], p[5], p[6], p[4]))
//                            if(!isSquareIntersect(a, p[0], p[2], p[1]))
//                            {
//                                double half = size / 2.;
//                                if(isInsidePoint(new Vector(a.x + half, a.y + half, a.z + half)))
//                                    return type.BLACK_VOXEL;
//                                else
//                                    return type.WHITE_VOXEL;
//                            }
//
//        return type.GRAY_VOXEL;
    }

    public type typeCube1(Vector a, double size)
    {
        Vector p[] = new Vector[7];
        p[0] = new Vector(a.x + size, a.y,        a.z);
        p[1] = new Vector(a.x,        a.y,        a.z + size);
        p[2] = new Vector(a.x + size, a.y,        a.z + size);
        p[3] = new Vector(a.x,        a.y + size, a.z);
        p[4] = new Vector(a.x + size, a.y + size, a.z);
        p[5] = new Vector(a.x,        a.y + size, a.z + size);
        p[6] = new Vector(a.x + size, a.y + size, a.z + size);

        Vector[][] l = new Vector[][]{new Vector[]{a,    p[0]},
                new Vector[]{a,    p[1]},
                new Vector[]{a,    p[3]},
                new Vector[]{p[2], p[0]},
                new Vector[]{p[4], p[0]},
                new Vector[]{p[1], p[2]},
                new Vector[]{p[1], p[5]},
                new Vector[]{p[2], p[6]},
                new Vector[]{p[3], p[4]},
                new Vector[]{p[3], p[5]},
                new Vector[]{p[4], p[6]},
                new Vector[]{p[5], p[6]}};

        long s = System.currentTimeMillis();

        for(int i = 0; i < 12; i++)
        {
            Iterator<ArrayList<Integer>> face = faces.iterator();
            Iterator<Vector> normal = normals1.iterator();

            TreeSet<Vector> in = new TreeSet<>();
            while(face.hasNext())
                if(inPolygon(face.next(), l[i], in, normal.next()))
                    return type.GRAY_VOXEL;
        }

//        System.out.println(sum/N + " " +  maxi + " " + mini);
//        if(isCubeIntersect(new Vector[] {a, p[6]}))
//            return type.GRAY_VOXEL;
//        f += System.currentTimeMillis() - s;
//        System.out.println( "F " +f );
        double half = size / 2.;
        if(isInsidePoint(new Vector(a.x + half, a.y + half, a.z + half)))
            return type.BLACK_VOXEL;
        else
            return type.WHITE_VOXEL;
    }

    public type typeCube2(Vector a, double size)
    {
        Vector p[] = new Vector[7];
        p[0] = new Vector(a.x + size, a.y,        a.z);
        p[1] = new Vector(a.x,        a.y,        a.z + size);
        p[2] = new Vector(a.x + size, a.y,        a.z + size);
        p[3] = new Vector(a.x,        a.y + size, a.z);
        p[4] = new Vector(a.x + size, a.y + size, a.z);
        p[5] = new Vector(a.x,        a.y + size, a.z + size);
        p[6] = new Vector(a.x + size, a.y + size, a.z + size);

        Vector[][] l = new Vector[][]{new Vector[]{a,    p[0]},
                new Vector[]{a,    p[1]},
                new Vector[]{a,    p[3]},
                new Vector[]{p[2], p[0]},
                new Vector[]{p[4], p[0]},
                new Vector[]{p[1], p[2]},
                new Vector[]{p[1], p[5]},
                new Vector[]{p[2], p[6]},
                new Vector[]{p[3], p[4]},
                new Vector[]{p[3], p[5]},
                new Vector[]{p[4], p[6]},
                new Vector[]{p[5], p[6]}};

//        long s = System.currentTimeMillis();

        for(int i = 0; i < 12; i++)
        {
            Iterator<ArrayList<Integer>> face = faces.iterator();
            Iterator<Vector> normal = normals1.iterator();

            TreeSet<Vector> in = new TreeSet<>();
            while(face.hasNext())
                if(inPolygon(face.next(), l[i], in, normal.next()))
                    return type.GRAY_VOXEL;
        }

//        System.out.println(sum/N + " " +  maxi + " " + mini);
//        if(isCubeIntersect(new Vector[] {a, p[6]}))
//            return type.GRAY_VOXEL;
//        f += System.currentTimeMillis() - s;
//        System.out.println( "F " +f );
        double half = size / 2.;
        if(isInsidePoint(new Vector(a.x + half, a.y + half, a.z + half)))
            return type.BLACK_VOXEL;
        else
            return type.WHITE_VOXEL;
    }

    public boolean isCubeIntersect(Vector[] AABB)
    {
        for(Integer[] line : lines)
        {
            Vector s = vertexes.get(line[0]);
            Vector e = vertexes.get(line[1]);
            Vector dir = s.subtract(e);
            double magnitude = dir.magnitude();
            dir = dir.multiply(1f/magnitude).invert();

            byte[] sign = new byte[3];
            sign[0] = (byte) ((dir.x < 0) ? 1 : 0);
            sign[1] = (byte) ((dir.y < 0) ? 1 : 0);
            sign[2] = (byte) ((dir.z < 0) ? 1 : 0);


            if(AABB_line(dir, e, magnitude, AABB, sign))
                return true;
        }

        return false;
    }
   /**
    * @param inv_dir 1/dir
    * @param origin positions start of vector
    * @param end magnitude line
    * @param bounds a and b point cube
    * @param sign sign of inv_dir x,y,z component
    */

    private boolean AABB_line(Vector inv_dir, Vector origin, double end, Vector[] bounds, byte[] sign)
    {
        double tmin, tmax, tymin, tymax, tzmin, tzmax;
        tmin =  (bounds[sign[0]].x   - origin.x) * inv_dir.x;
        tmax =  (bounds[1-sign[0]].x - origin.x) * inv_dir.x;
        tymin = (bounds[sign[1]].y   - origin.y) * inv_dir.y;
        tymax = (bounds[1-sign[1]].y - origin.y) * inv_dir.y;

        if ( (tmin > tymax) || (tymin > tmax) )
            return false;

        if (tymin > tmin)
            tmin = tymin;

        if (tymax < tmax)
            tmax = tymax;

        tzmin = (bounds[sign[2]].z   - origin.z) * inv_dir.z;
        tzmax = (bounds[1-sign[2]].z - origin.z) * inv_dir.z;

        if ( (tmin > tzmax) || (tzmin > tmax) )
            return false;

        if (tzmin > tmin)
            tmin = tzmin;

        if (tzmax < tmax)
            tmax = tzmax;

        return (tmin < tmax) && (tmax > 0.f) && (tmax < end);
    }

    public boolean isSquareIntersect(Vector a, Vector b, Vector c, Vector d)
    {
        Vector a1 = a.subtract(c);
        Vector b1 = b.subtract(c);
        Vector normal = a1.cross(b1).normalize();
        Vector[] face = new Vector[]{a, b, c ,d};

        for(Integer[] line : lines)
            if(inSquare(face, new Vector[]{vertexes.get(line[0]), vertexes.get(line[1])}, normal))
                return true;

        return false;
    }



    public boolean isInsidePoint(Vector a)
    {
        Vector[] l = new Vector[]{min, a};

        TreeSet<Vector> in = new TreeSet<>();

        Iterator<ArrayList<Integer>> face = faces.iterator();
        Iterator<Vector> normal = normals1.iterator();

        while(face.hasNext())
           inPolygon(face.next(), l, in, normal.next());

        return (in.size() & 1) == 1;
    }

    private Double isIntersectedPlane(Vector D, Vector line[], Vector normal)
    {
        double distance1, distance2;

        double originDistance = - normal.dot(D);

        distance2 = normal.dot(line[1]) + originDistance;

        distance1 = normal.dot(line[0]) + originDistance;
        if(distance1 * distance2 >= 0)
            return null;

        return originDistance;
    }

    private Vector intersectionPoint(Vector normal, Vector line[], double distance)
    {
        Vector line_dir;
        double numerator, denominator, dist;

        line_dir = line[1].subtract(line[0]);
        line_dir = line_dir.normalize();

        numerator = - normal.dot(line[0]) - distance;
        denominator = normal.dot(line_dir);


        if(denominator == 0)
            return line[0];

        dist = numerator / denominator;

        return line[0].add(line_dir.multiply(dist));
    }

    static final double PI_2eps = 6.283184;
//    static final double PI_2    = 6.283186;
    static final double PI_2    = 6.2831853071795862;

    private boolean insidePolygon(Vector intersect, ArrayList<Integer> face)
    {
        double angle = 0;
        Iterator<Integer> it = face.iterator();

        Vector a = vertexes.get(it.next()).subtract(intersect);
        Vector b, c = a.clone();
        it.next(); it.next();

        while (it.hasNext())
        {
            b = a.clone();
            a = vertexes.get(it.next()).subtract(intersect);
            it.next(); it.next();
            angle += angleBetweenVectors(b, a);
        }
        angle += angleBetweenVectors(c, a);

//        return angle > PI_2eps && angle < PI_2;
        return  (Math.abs(angle - PI_2)  < 0.000001);
    }

    //Частный случай для квадрата
    private boolean insidePolygon(Vector intersect, Vector[] face)
    {
        double angle = 0;

        Vector a = face[0].subtract(intersect);
        Vector b = face[1].subtract(intersect);
        Vector c = face[2].subtract(intersect);
        Vector d = face[3].subtract(intersect);

        angle += angleBetweenVectors(a, b);
        angle += angleBetweenVectors(b, c);
        angle += angleBetweenVectors(c, d);
        angle += angleBetweenVectors(d, a);

        return angle > PI_2eps && angle < PI_2;
    }

    private double angleBetweenVectors(Vector a, Vector b)
    {
        double dotProduct = a.dot(b);
        double vectorsMagnitude = a.magnitude() * b.magnitude();

        if(vectorsMagnitude == 0)
            return 0;

        double angle = acos(dotProduct / vectorsMagnitude);

        return Double.isNaN(angle) ? 0 : angle;
    }

    private boolean inPolygon(ArrayList<Integer> face, Vector line[], TreeSet<Vector> in, Vector next)
    {
        Vector normal = next;
        Double distance = isIntersectedPlane(vertexes.get(face.get(0)), line, next);

        if(distance == null)
            return false;

        Vector intersect = intersectionPoint(normal, line, distance);

        return insidePolygon(intersect, face) && in.add(intersect);
    }

    private boolean inSquare(Vector[] face, Vector line[], Vector normal)
    {
        Double distance = isIntersectedPlane(face[0], line, normal);

        if(distance == null)
            return false;

        Vector intersect = intersectionPoint(normal, line, distance);

	    return insidePolygon(intersect, face);
    }

    public void draw(GL2 gl)
    {
        GLUT glut = new GLUT();
        float siz1 = 0.5f;
        Iterator<type> it = type_quad.iterator();

        for(Vector point : quad)
        {
            gl.glPushMatrix();
            gl.glTranslated(point.x + siz1, point.y + siz1, point.z + siz1);
            switch (it.next())
            {
                case GRAY_VOXEL:
                    gl.glColor4d(1, 0, 0, 1);
                    glut.glutSolidCube(1);
                    gl.glPopMatrix();
                    break;
                case BLACK_VOXEL:
                    gl.glColor4d(0, 0, 1, 0.7);
                    glut.glutSolidCube(1);
                    gl.glPopMatrix();
                    break;
                case WHITE_VOXEL:
//                    gl.glColor4d(1, 0, 0, 0.2);
//                    glut.glutSolidCube(8);
                    gl.glPopMatrix();
                    break;
            }

        }
    }

    /**
     * Transparent cube volume. If cube volume into mesh, then no transperent (0).
     * @param vector begin point of cube
     * @param size size cube
     * @return value of transperent (0...255).
     */
    public int transparentCube(Vector vector, double size)
    {
        int tr = 0;

        size /= 8;

        for (int i = 0; i < 7; i++)
            for (int j = 0; j < 7; j++)
                for (int k = 0; k < 7; k++)
                    if(isInsidePoint(vector.add(new Vector(i * size, j * size, k * size))))
                        tr++;

        return tr > 2 ? tr / 2 - 1 : 0;
    }
}
