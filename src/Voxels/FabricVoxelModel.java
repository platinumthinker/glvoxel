package Voxels;

import Mesh.Mesh;
import Mesh.Vector;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 15.09.12
 * Time: 20:46
 */

public class FabricVoxelModel
{
    public VoxelModel generate(Mesh mesh, int step)
    {
        TestIntersect test = new TestIntersect(mesh);

        GrayVoxel root = new GrayVoxel();

        double size = mesh.size - 0.00001;
        Vector pos = mesh.min.clone();
        pos.y = mesh.center.y - size /2;
        pos.subtract(0.00001, 0.00001, 0.00001);
        double size1 = size / 2.;
        int step1 = step - 1;

        Vector p[] = new Vector[8];
        p[0] = pos.clone();
        p[1] = new Vector(pos.x + size1, pos.y,         pos.z);
        p[2] = new Vector(pos.x,         pos.y,         pos.z + size1);
        p[3] = new Vector(pos.x + size1, pos.y,         pos.z + size1);
        p[4] = new Vector(pos.x,         pos.y + size1, pos.z);
        p[5] = new Vector(pos.x + size1, pos.y + size1, pos.z);
        p[6] = new Vector(pos.x,         pos.y + size1, pos.z + size1);
        p[7] = new Vector(pos.x + size1, pos.y + size1, pos.z + size1);

        byte number = 0;
        Voxel[] child = new Voxel[8];

        ArrayList<Thread> calc = new ArrayList<>();

        for (int i = 0; i < 8; i++)
        {
            GrayVoxel buf = new GrayVoxel();
            child[7 - i] = buf;
            calc.add(new Compositer(test, buf, p[7 - i], size1, step1));
            number |= 1 << i;
        }

        for(Thread thread : calc)
            while (thread.isAlive())
                Thread.yield();

        root.setChildren(child, number);

        return  new VoxelModel(root, (byte) step, size, pos);
    }
}

class Compositer extends Thread
{
    private boolean DEBUG_COLOR = true;
    TestIntersect test;
    GrayVoxel a;
    Vector begin;
    double size;
    int step;
    long B = 0, G = 0, W = 0, res;

    public Compositer(TestIntersect test, GrayVoxel a, Vector begin, double size, int step)
    {
        super();

        this.test = test;
        this.a = a;
        this.begin = begin;
        this.size = size;
        this.step = step;
        
        start();
    }
    
    @Override
    public void run()
    {
        super.run();
        for (int i = 0; i <= step; i++)
            res += (long) Math.pow(8, i);

        composite(a, begin, size, step);
    }

    private void composite(GrayVoxel a, Vector begin, double size, int step)
    {
        if(step <= 0)
            return;

        step--;
        size /= 2;
        
        Vector p[] = new Vector[8];
        p[0] = begin.clone();
        p[1] = new Vector(begin.x + size, begin.y,        begin.z);
        p[2] = new Vector(begin.x,        begin.y,        begin.z + size);
        p[3] = new Vector(begin.x + size, begin.y,        begin.z + size);
        p[4] = new Vector(begin.x,        begin.y + size, begin.z);
        p[5] = new Vector(begin.x + size, begin.y + size, begin.z);
        p[6] = new Vector(begin.x,        begin.y + size, begin.z + size);
        p[7] = new Vector(begin.x + size, begin.y + size, begin.z + size);

        byte number = 0;
        Voxel[] child = new Voxel[8];
        if(step == 0)
            for (int i = 0; i < 8; i++)
                switch (test.typeCube(p[7 - i], size))
                {
                    case GRAY_VOXEL:
                        int tr = test.transparentCube(p[7 - i], size);
                        child[7 - i] = new BlackVoxel(new Color(255, 255, 255, tr));
                        number |= 1 << i;
                        B++;
                        break;
                    case BLACK_VOXEL:
                        child[7 - i] = new BlackVoxel(Color.WHITE);
                        number |= 1 << i;
                        B++;
                        break;
                    case WHITE_VOXEL:
                        W++;
                        break;
                }
        else
        for (int i = 0; i < 8; i++)
            switch (test.typeCube(p[7 - i], size))
            {
                case GRAY_VOXEL:
                    GrayVoxel buf = new GrayVoxel();
                    child[7 - i] = buf;
                    composite(buf, p[7 - i], size, step);
                    number |= 1 << i;
                    G++;
                    break;
                case BLACK_VOXEL:
                    child[7 - i] = new BlackVoxel(Color.WHITE);
                    number |= 1 << i;
                    B++;
                    break;
                case WHITE_VOXEL:
                    W++;
                    break;
            }

        a.setChildren(child, number);
    }

    private Color getColor(int n)
    {
        Color color = null;
        switch (n)
        {
            case 0: color = new Color(255, 177, 5,   255); break;
            case 1: color = new Color(37,  44,  255, 255); break;
            case 2: color = new Color(220, 93,  255, 255); break;
            case 3: color = new Color(64,  236, 255, 255); break;
            case 4: color = new Color(27,  161, 28 , 255); break;
            case 5: color = new Color(255, 253, 44 , 255); break;
            case 6: color = new Color(255, 255, 255, 255); break;
            case 7: color = new Color(211, 148, 81 , 255); break;
        }
        return DEBUG_COLOR ? color : Color.WHITE;
    }
}