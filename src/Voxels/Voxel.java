package Voxels;

import Mesh.Vector;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 15.09.12
 * Time: 18:51
 */

/**
 *  The <code>Voxel</code> is node in octree.
 *  Voxel has been {@link BlackVoxel}, {@link GrayVoxel}, WhiteVoxel and {@link VoxelModel}.
 */
public abstract class Voxel
{
    public static enum type {
                                GRAY_VOXEL,
                                WHITE_VOXEL,
                                BLACK_VOXEL,
                                VOXEL_MODEL,
                                VOXEL_BUFFER
                            }

    public Voxel(){}

    public void draw(GL2 gl, GLUT glu, Vector point, double size, int level){}
    public void draw(GL2 gl, GLUT glu){}

	/**
	 * Type of this voxel.
	 * @return GRAY_VOXEL, BLACK_VOXEL, WHITE_VOXEL, VOXEL_MODEL.
	 */
    public abstract type getType();

	/**
	 * Cast this voxel to Black Voxel.
	 * @return BlackVoxel or if this not Black Voxel return null.
	 */
    public abstract BlackVoxel getBVoxel();

	/**
	 * Cast this voxel to Gray Voxel.
	 * @return GrayVoxel or if this not Gray Voxel return null.
	 */
	public abstract GrayVoxel getGVoxel();

	/**
	 * Color this voxel. If this is Gray Voxel that calculate complex color.
	 * @return byte array contains RGBA color components.
	 */
    public abstract byte[] getColor();
}
