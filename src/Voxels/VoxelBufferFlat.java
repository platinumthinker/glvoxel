package Voxels;

import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;
import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 20.10.12
 * Time: 1:29
 */

public class VoxelBufferFlat extends VoxelModel
{
    public byte[] childBuffer;
    public int[] indexes;
    public int[] colorBuffer;
    ByteBuffer buffer;
    public int n;

    public VoxelBufferFlat(VoxelModel voxelModel)
    {
        buffer = ByteBuffer.allocate(4);
        fillBuffers(voxelModel);
    }

    public void fillBuffers(VoxelModel model)
    {
        this.level = model.level;
        this.leveldraw = 1;
        this.point = model.point;
        this.size = model.size / (1 << level);
        root = model.getRoot();

        indexes = new int[level];
        n = 1;
        int aa = 0;
        for(int i = 0; i < level; i++, n <<= 1)
        {
            indexes[i] = n;
            aa += n * n * n;
        }

        childBuffer = new byte[level * aa];

        colorBuffer = new int[n * n * n];

        fillLevel(root, 0, 0, 0, 0);
    }

    private void fillLevel(Voxel voxel, int level, int x, int y, int z)
    {
        switch (voxel.getType())
        {
            case GRAY_VOXEL:
                int index = indexes[level];
                childBuffer[((level++ * index + x ) * index + y ) * index + z] = voxel.getGVoxel().getChildren();
                GrayVoxelIterator it = voxel.getGVoxel().iterator();

                while (it.hasNext())
                {
                    Voxel v = it.next(); int num[] = it.getCoordinate(x, y, z);
                    fillLevel(v, level, num[0], num[1], num[2]);
                }
                break;
            case BLACK_VOXEL:
                if(this.level == level)
                    colorBuffer[(x * n + y) * n + z] = voxel.getBVoxel().getColorInt();
                else
                {
                    int color = voxel.getBVoxel().getColorInt();
                    int n = 1 << (this.level - level);
                    for(int i = x * n; i < x*n + n; i++)
                        for(int j = y * n; j < y*n + n; j++)
                            for(int k = z * n; k < z*n + n; k++)
                                colorBuffer[(i * n + j) * n + k] = color;
                }
                break;
        }
    }

    @Override
    public type getType()
    {
        return type.VOXEL_BUFFER;
    }

    @Override
    public void draw(GL2 gl, GLUT glu)
    {
        //Nothing
    }
}