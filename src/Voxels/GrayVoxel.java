package Voxels;

import Mesh.Vector;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 15.09.12
 * Time: 20:32
 */

/**
 * Gray Voxel is node in octree. Contains others voxels.
 * Order of placement :
 * down:
 * 2 3
 * 0 1
 * up:
 * 6 7
 * 4 5
 */

public class GrayVoxel extends Voxel implements Iterable<Voxel>
{
    private Voxel[] children;

	/**
	 * number is mask of child: 0 1 2 3 4 5 6 7
	 */
	private byte number;
    public static boolean DRAW_AVR_COLOR = false;
    public static boolean DRAW_GRAY_VOXEL = true;

    public GrayVoxel()
    {
        number = 0;
    }

    public GrayVoxel(Voxel[] children, byte number)
    {
        this.number = number;
        this.children = children;
    }

    public void setChildren(Voxel[] children, byte numberChildren)
    {
        this.children = children;
        this.number = numberChildren;
    }

    @Override
    public void draw(GL2 gl, GLUT glut, Vector point, double size, int level)
    {
        size /= 2.0f;

        if(level-- <= 0)
        {
            if(DRAW_AVR_COLOR)
            {
                gl.glPushMatrix();
                gl.glTranslated(point.x + size, point.y + size, point.z + size);
                byte[] color = getTransparent();
//                gl.glColor4ub(color[0], color[1], color[2], color[3]);
                gl.glColor4ubv(color, 0);
                glut.glutSolidCube((float) size * 2);
                gl.glPopMatrix();
            }
            return;
        }

        if(DRAW_GRAY_VOXEL)
        {
            gl.glLineWidth(0.4f);
            gl.glPushMatrix();
            gl.glTranslated(point.x + size, point.y + size, point.z + size);
            gl.glColor3d(1., .0, .0);
            glut.glutWireCube((float) size * 2);
            gl.glPopMatrix();
        }

        GrayVoxelIterator it = this.iterator();

	    while (it.hasNext())
        {
            Voxel v = it.next();
            if(v.getType() == type.BLACK_VOXEL)
	            v.getBVoxel().draw(gl, glut, it.getVector(point, size), size, level, number);
            else
                v.draw(gl, glut, it.getVector(point, size), size, level);
        }
    }

    @Override
    public type getType()
    {
        return type.GRAY_VOXEL;
    }

    @Override
    public BlackVoxel getBVoxel()
    {
        return null;
    }

    @Override
    public GrayVoxel getGVoxel()
    {
        return this;
    }

    public byte getChildren()
    {
        return number;
    }

    public Voxel[] getChildrenVoxel()
    {
        return children;
    }

    @Override
    public byte[] getColor()
    {
        int R = 0, G = 0, B = 0, A = 0;
        byte[] color;
	    short col = 0;

	    for(Voxel voxel : this)
	    {
		    color = voxel.getColor();
		    R += (int)color[0] & 0xFF;
		    G += (int)color[1] & 0xFF;
		    B += (int)color[2] & 0xFF;
		    A += (int)color[3] & 0xFF;
		    col++;
	    }

        return new byte[]{  (byte) (R/col),
		                    (byte) (G/col),
		                    (byte) (B/col),
		                    (byte) (A/col)};
    }

	@Override
	public GrayVoxelIterator iterator()
	{
		return new GrayVoxelIterator(this);
	}

    public byte[] getTransparent()
    {
        int A = 0;
        byte[] color;
        short col = 0;

        for(Voxel voxel : this)
        {
            color = voxel.getColor();
            A += (int)color[3] & 0xFF;
            col++;
        }

        A /= col;
        return new byte[]{  (byte) 255,
                            (byte) 0,
                            (byte) 0,
                            (byte) A};
    }
}
