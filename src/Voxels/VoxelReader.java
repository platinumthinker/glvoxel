package Voxels;

import Mesh.Vector;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 13.10.12
 * Time: 3:09
 */
public class VoxelReader
{
    private BufferedOutputStream out;
    private BufferedInputStream input;
    public final static int GRAY_VOXEL_IND  = 0b0,
                            BLACK_VOXEL_IND = 0b10;

    private long B, G;
    final static byte WORK_DATA = (byte) 0b1100_1100;

    public void save(VoxelModel voxel, String name) throws FileNotFoundException
    {
//        out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(new File(name))));
//        out.setLevel(9);
//        out.setMethod(ZipOutputStream.DEFLATED);
//        ZipEntry target = new ZipEntry("a");
//        try
//        {
//            out.putNextEntry(target);
//        } catch (IOException e)
//        {
//            e.printStackTrace();
//        }
        out = new BufferedOutputStream(new FileOutputStream(name));
        try
        {
            out.write(voxel.level);
        } catch (IOException ignored){}

        B = G = 0;

        try
        {
            save_voxel(voxel.getRoot());

            out.write(WORK_DATA);

            out.write(voxel.getSize());
            out.write(voxel.getPoint());

            out.write(toByteArray(B));
            out.write(toByteArray(G));

            out.flush();
            out.close();
        } catch (IOException ignored){}
    }

    private void save_voxel(Voxel voxel) throws IOException
    {
        switch (voxel.getType())
        {
            case BLACK_VOXEL:
                out.write(BLACK_VOXEL_IND);
                out.write(voxel.getBVoxel().getColor(), 0, 4);
                B++;
                break;
            case GRAY_VOXEL:
                out.write(GRAY_VOXEL_IND);
                out.write(voxel.getGVoxel().getChildren());

	            int num = 0;
	            for(Voxel children : voxel.getGVoxel())
	                save_voxel(children);

                out.flush();
                G++;
                break;
        }
    }

    public VoxelModel read(String name) throws IOException
    {
//        input = new ZipInputStream(new BufferedInputStream(new FileInputStream(new File(name))));
//        input.getNextEntry();
        input = new BufferedInputStream(ClassLoader.getSystemResourceAsStream(name));
        byte level = 0;
        try
        {
            level = (byte) input.read();
            if(level == -1)
                return null;
        } catch (IOException ignored){}

        GrayVoxel root = null;

        B = G = 0;
        try
        {
            root = read_voxel().getGVoxel();
        }catch(IOException ignored){}

        byte b1[] = new byte[8],
             g1[] = new byte[8],
             w1[] = new byte[8],
             size_byte[] = new byte[8],
             point_byte[] = new byte[24];

        byte type = (byte) input.read();
        if(type == WORK_DATA)
        {
            input.read(size_byte);
            input.read(point_byte);

            input.read(b1);
            input.read(g1);

            if(toLong(b1) != B)
                throw new IOException("Error read file. File is corrupted. " +
                        "Count of black voxels " + B + ", but should be " + toLong(b1));

            if(toLong(g1) != G)
                throw new IOException("Error read file. File is corrupted. " +
                        "Count of black voxels " + G + ", but should be " + toLong(g1));
        }
        else
            throw new IOException("Error read block WORK_DATA.");


        return new VoxelModel(root, level, toDouble(size_byte), toVector(point_byte));
    }

    private Voxel read_voxel() throws IOException
    {
        Voxel voxel = null;
        byte type = (byte) input.read();
        switch (type)
        {
            case BLACK_VOXEL_IND:
                byte[] color = new byte[4];
                input.read(color, 0, 4);
                voxel = new BlackVoxel(color);
                B++;
                break;
            case GRAY_VOXEL_IND:
                byte number = (byte) input.read();
                Voxel[] children = new Voxel[8];
                int b = number;

                int num = 0;
                int y = 0;


                while (b != 0)
                {
                    if((b & 0b1000_0000) != 0)
                        children[num] = read_voxel();
                    else
                        y++;
                    b <<= 1;
                    num++;
                }

                voxel = new GrayVoxel(children, number);

                G++;
                break;
        }

        return voxel;
    }

    private byte[] toByteArray(long value)
    {
        ByteBuffer bb = ByteBuffer.allocate(8);
        return bb.putLong(value).array();
    }

    private long toLong(byte[] array)
    {
        return ByteBuffer.wrap(array).getLong();
    }

    private Vector toVector(byte[] array)
    {
        return new Vector(ByteBuffer.wrap(array).getDouble(),
                          ByteBuffer.wrap(array).getDouble(8),
                          ByteBuffer.wrap(array).getDouble(16));
    }

    private double toDouble(byte[] array)
    {
        return ByteBuffer.wrap(array).getDouble();
    }
}
