package Voxels;

import Mesh.Vector;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;
import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 25.09.12
 * Time: 21:13
 */


public class VoxelModel extends Voxel
{
    protected GrayVoxel root;
    byte level;
    byte leveldraw;
    public double size;
    public Vector point;

    public VoxelModel(GrayVoxel root, byte level, double size, Vector point)
    {
        this.root = root;
        this.level = level;
        this.size = size;
        this.point = point;
        this.leveldraw = 2;
    }

	public VoxelModel() {}

	@Override
    public void draw(GL2 gl, GLUT glu)
    {
//        gl.glBegin(GL2.GL_POINTS);
        root.draw(gl, glu, point, size, leveldraw);
//        gl.glEnd();

    }

    public byte getLevel()
    {
        return level;
    }

    @Override
    public type getType()
    {
        return type.VOXEL_MODEL;
    }

    @Override
    public BlackVoxel getBVoxel()
    {
        return null;
    }

    @Override
    public GrayVoxel getGVoxel()
    {
        return null;
    }

    @Override
    public byte[] getColor()
    {
        return null;
    }

    public void levelUp()
    {
        if(leveldraw < level)
            leveldraw++;
	    System.out.println(leveldraw);
    }

    public void levelDown()
    {
        if(leveldraw > 1)
            leveldraw--;
	    System.out.println(leveldraw);
    }

    public GrayVoxel getRoot()
    {
        return root;
    }

    public byte[] getSize()
    {
        ByteBuffer bb = ByteBuffer.allocate(8);
        return bb.putDouble(size).array();
    }

    public byte[] getPoint()
    {
        ByteBuffer bb = ByteBuffer.allocate(24);
        bb = bb.putDouble(point.x);
        bb = bb.putDouble(point.y);
        return bb.putDouble(point.z).array();
    }

    public void setPoint(Vector point)
    {
        this.point = point;
    }

}
