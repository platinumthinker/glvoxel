package Voxels;
// created by: 10/23/12 2:01 PM programmer: thinker

import Mesh.Vector;

import java.util.Iterator;

/**
 * Iterator for children gray voxel's.
 */
public class GrayVoxelIterator implements Iterator<Voxel>
{
	private byte number, index;
	private Voxel[] mas;

	public GrayVoxelIterator(GrayVoxel voxel)
	{
	  	mas = voxel.getChildrenVoxel();
		number = voxel.getChildren();
		index = -1;
	}

	@Override
	public boolean hasNext()
	{
		return number != 0;
	}

	@Override
	public Voxel next()
	{
		boolean hasNext;

		do
		{
			hasNext = (number & 0b1000_0000) == 0;
			index++;
			number <<= 1;
		}
		while(hasNext);

		return mas[index];
	}

	@Override
	public void remove()
	{
		next();
	}

	public byte getIndex()
	{
		return index;
	}

	public Vector getVector(Vector point, double size)
	{
		Vector p = point.clone();

        switch (index)
        {
            case 0: break;
            case 1: p.x += size; break;
            case 2: p.z += size; break;
            case 3: p.x += size; p.z += size; break;
	        case 4: p.y += size; break;
	        case 5: p.y += size; p.x += size; break;
	        case 6: p.y += size; p.z += size; break;
	        case 7: p.y += size; p.x += size; p.z += size; break;
        }

		return p;
	}

	public int[] getCoordinate(int x, int y, int z)
	{
		x <<= 1; y <<= 1; z <<= 1;

		switch (index)
		{
			case 0: break;
			case 1: x++; break;
			case 2: z++; break;
			case 3: x++; z++; break;
			case 4: y++; break;
			case 5: y++; x++; break;
			case 6: y++; z++; break;
			case 7: y++; x++; z++; break;
		}

		return new int[]{x, y, z};
	}
}
