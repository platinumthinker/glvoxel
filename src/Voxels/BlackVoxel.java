package Voxels;

import Mesh.Vector;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.GL2;
import java.awt.*;
import java.nio.ByteBuffer;
import java.util.TreeMap;


/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 15.09.12
 * Time: 20:26
 */

/**
* Black Voxel is leaf in octree.
* Color ARGB model.
*/

public class BlackVoxel extends Voxel
{
    byte A, R, G, B;
    private static TreeMap<Byte, byte[]> colMap;

    static
    {
        colMap = new TreeMap<>();
        byte C = 0;

        for(int i = -128; i < 128; i++)
            colMap.put((byte)i, new byte[]{C, C, C++});
    }

    public BlackVoxel(Color color)
    {
        int col = color.getRGB() | color.getAlpha() << 24;

        A = getAlpha(col);
        R = getRed(col);
        G = getGreen(col);
        B = getBlue(col);
    }

    public BlackVoxel(int color)
    {
        A = getAlpha(color);
        R = getRed(color);
        G = getGreen(color);
        B = getBlue(color);
    }

    public BlackVoxel(byte[] color)
    {
        setColor(color);
    }

//    @Override
    public void draw(GL2 gl, GLUT glu, Vector point, double size, int level, byte col)
    {
        double siz1 = size/2;
        gl.glPushMatrix();
        gl.glTranslated(point.x + siz1, point.y + siz1, point.z + siz1);
        gl.glColor4ub((byte)255, (byte)0, (byte)0, A);
//          gl.glColor3ubv(colMap.get(col), 0);

//        gl.glColor3ub(A, A, A);
//            gl.glPointSize((float)size);
//            gl.glVertex3d(point.x, point.y, point.z);
        glu.glutSolidCube((float) size);
        gl.glPopMatrix();
    }

    @Override
    public type getType()
    {
        return type.BLACK_VOXEL;
    }

    @Override
    public BlackVoxel getBVoxel()
    {
        return this;
    }

    @Override
    public GrayVoxel getGVoxel()
    {
        return null;
    }

    @Override
    public byte[] getColor()
    {
        return new byte[]{R, G, B, A};
    }

	public int getColorInt()
	{
		return ByteBuffer.wrap(new byte[]{R, G, B, A}).getInt();
	}

    public byte getAlpha(int color)
    {
        return (byte) (color >> 24 & 0xff);
    }

    public byte getRed(int color)
    {
        return (byte) (color >> 16 & 0xff);
    }


    public byte getGreen(int color)
    {
        return (byte) (color >> 8 & 0xff);
    }

    public byte getBlue(int color)
    {
        return (byte) (color & 0xff);
    }

    public void setColor(byte[] color)
    {
        R = color[0];
        G = color[1];
        B = color[2];
        A = color[3];
    }
}
