package Mesh;


/**
 *
 * @author thinker
 */

public class Vector implements Comparable<Vector>
{
    private static float eps = 0.00001f;
    public double x,y,z;
    public Vector(){}

    public Vector(double x, double y, double z)
    {
        set(x, y, z);
    }

    @SuppressWarnings("SuspiciousNameCombination")
    public Vector(double x)
    {
        set(x, x, x);
    }

    public Vector clone()
    {
        return new Vector(x, y, z);
    }

    public void set(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void add(double x, double y, double z)
    {
        this.x += x;
        this.y += y;
        this.z += z;
    }

    public void multiply(double x, double y, double z)
    {
        this.x *= x;
        this.y *= y;
        this.z *= z;
    }

    @Override
    public String toString()
    {
        return x + " " + y + " " + z;
    }

    public void subtract(double x, double y, double z)
    {
        this.x -= x;
        this.y -= y;
        this.z -= z;
    }

    public void divide(double x, double y, double z)
    {
        this.x /= x;
        this.y /= y;
        this.z /= z;
    }

    public Vector ceil()
    {
        return new Vector(Math.ceil(x), Math.ceil(y), Math.ceil(z));
    }
    public Vector subtract(Vector v)
    {
        return new Vector(this.x - v.x, this.y - v.y, this.z - v.z);
    }

    public Vector add(Vector v)
    {
        return new Vector(this.x + v.x, this.y + v.y, this.z + v.z);
    }

    public Vector multiply(Vector v)
    {
        return new Vector(this.x * v.x, this.y * v.y, this.z * v.z);
    }

    public Vector multiply(double v)
    {
        return new Vector(this.x * v, this.y * v, this.z * v);
    }

    public Vector divide(Vector v)
    {
        return new Vector(this.x / v.x, this.y / v.y, this.z / v.z);
    }

    public Vector invert()
    {
        return new Vector(1/x, 1/y, 1/z);
//        return new Vector((this.x != 0) ? 1. / this.x : 0,
//                          (this.y != 0) ? 1. / this.y : 0,
//                          (this.z != 0) ? 1. / this.z : 0);
    }

    public Vector cross(Vector v)
    {
        Vector vNormal= new Vector();

        vNormal.x = ((y * v.z) - (z * v.y));
        vNormal.y = ((z * v.x) - (x * v.z));
        vNormal.z = ((x * v.y) - (y * v.x));

        return vNormal;
    }

    public double magnitude()
    {
        return Math.sqrt((x * x) + (y * y) + (z * z));
    }

    public Vector normalize()
    {
        double magnitude = magnitude();
        if(magnitude != 0)
            return this.multiply(1f/magnitude);
        else
            return this.clone();
    }

    public void max(Vector a)
    {
        x = Math.max(x, a.x);
        y = Math.max(y, a.y);
        z = Math.max(z, a.z);
    }

    public void min(Vector a)
    {
        x = Math.min(x, a.x);
        y = Math.min(y, a.y);
        z = Math.min(z, a.z);
    }

    public boolean isIntoCube(Vector a, Vector b)
    {
        return (x >= a.x && x <= b.x) && (y >= a.y && y <= b.y)
                && (z >= a.z && z <= b.z);
    }

    public double dot(Vector a)
    {
        return x*a.x + y*a.y + z*a.z;
    }

    public boolean equals(Vector a)
    {
        return  a.x - eps < x && a.x + eps > x &&
                a.y - eps < y && a.y + eps > y &&
                a.z - eps < z && a.z + eps > z;
    }

    @Override
    public int compareTo(Vector a)
    {
        if(equals(a)) return 0;

        if(a.x > x) return -1;
        if(a.x < x) return 1;
        if(a.y > y) return -1;
        if(a.y < y) return 1;
        if(a.z > z) return -1;
        return 1;
    }
}
