package Mesh;

import javax.media.opengl.GL2;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import static java.lang.Math.max;
import static javax.media.opengl.GL2.*;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 15.09.12
 * Time: 21:04
 */

public class Mesh
{
    ArrayList<Vector> vertexs, normals, texturesCoord;
    ArrayList<ArrayList<Integer>> face;
    public Vector max, min, center;
    public double size = 0;
    public Vector pos = new Vector(0,0,0);
    public Mesh()
    {
        vertexs = new ArrayList<>();
        normals = new ArrayList<>();
        texturesCoord = new ArrayList<>();
        face = new ArrayList<>();
    }

    public void LoadFromFile(String path)
    {
        max = new Vector(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE);
        min = new Vector(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE);

        InputStream is = new BufferedInputStream(ClassLoader.getSystemResourceAsStream(path));
        Scanner sc = new Scanner(is);
        sc.useLocale(Locale.US);
        Vector a;

        while(sc.hasNext())
        {
            String ind = sc.next();

            if(!sc.hasNextLine())
                break;
            switch (ind.charAt(0))
            {
                case 'v':
                    if (ind.length()==1)
                    {
                       a = new Vector(sc.nextFloat(), sc.nextFloat(), sc.nextFloat());
                       vertexs.add(a);
                       max.max(a);
                       min.min(a);
                    }
                    else
                    {
                        if (ind.charAt(1) == 'n')
                            normals.add(new Vector(sc.nextFloat(), sc.nextFloat(), sc.nextFloat()));
                        else
                            if (ind.charAt(1) == 't')
                                texturesCoord.add(new Vector(sc.nextFloat(), sc.nextFloat(), sc.nextFloat()));
                    }
                    break;
                case 'f':
                        FaceAdd(sc.nextLine());
                    break;
                default:
                        break;
            }
        }

        center = max.subtract(min);
        size = max(center.x, max(center.y, center.z));

        center = max.add(min);
        center.divide(2.f, 2.f, 2.f);

        for(Vector v : vertexs)
            v.subtract(center.x, center.y, center.z);

        min.subtract(center.x, center.y, center.z);
        max.subtract(center.x, center.y, center.z);
        center.set(0,0,0);

        scale(1./size);

    }

    void FaceAdd(String string)
    {
        Scanner sc = new Scanner(string);
        ArrayList<Integer> buf = new ArrayList<>();
        while(sc.hasNext())
        {
            String sub = sc.next();
            Scanner sub_sc = new Scanner(sub);
            sub_sc=sub_sc.useDelimiter("/\\s*");
            while(sub_sc.hasNextInt())
                buf.add(sub_sc.nextInt() - 1);
        }
        face.add(buf);
    }

    public void scale(double factor)
    {
        if(factor == 0)
            return;

        for(int i = 0; i < vertexs.size(); i++)
            vertexs.get(i).multiply(factor, factor, factor);

        min.multiply(factor, factor, factor);
        max.multiply(factor, factor, factor);
        size *= factor;
    }

    public void draw(GL2 gl)
    {

//        float mat_specular[]={0.7f,0.7f,0.7f};
//        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, mat_specular, 0);

        gl.glLineWidth(2f);
        gl.glColor3f(1f, 0f, 1f);
        gl.glPolygonMode(GL_FRONT, GL_FLAT);

        gl.glPushMatrix();
        gl.glTranslated(pos.x, pos.y, pos.z);

        for (ArrayList<Integer> i : face)
        {
            gl.glBegin(GL_TRIANGLES);
            for (int j = 0; j < i.size()/3; j++)
            {
                Vector v = normals.get(i.get(j*3+2));
                gl.glNormal3d(v.x, v.y, v.z);
                v = texturesCoord.get(i.get(j*3+1));
                gl.glTexCoord3d(v.x, v.y, v.z);
                v = vertexs.get(i.get(j*3));
                gl.glVertex3d(v.x, v.y, v.z);
            }
            gl.glEnd();
        }

        gl.glPopMatrix();

        gl.glPolygonMode(GL_FRONT, GL_FILL);
    }


    public ArrayList<Vector> getVertexs()
    {
        return vertexs;
    }

    public ArrayList<ArrayList<Integer>> getFace()
    {
        return face;
    }

    public ArrayList<Vector> getNormals()
    {
        return normals;
    }


}