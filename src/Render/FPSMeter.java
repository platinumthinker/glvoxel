package Render;

import com.jogamp.opengl.util.awt.TextRenderer;

import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import java.awt.*;

public class FPSMeter implements GLEventListener
{
    private long t0;
    private long t1;
    private double fps;
    private int frames;
    private TextRenderer textRenderer;
    
    public FPSMeter() 
    {}

    public void init(GLAutoDrawable drawable)
    {
        textRenderer = new TextRenderer(new Font("Default", Font.PLAIN, 20));
    }

	@Override
	public void dispose(GLAutoDrawable drawable)
	{
	}

	public void display(GLAutoDrawable drawable)
    {
        frames++;
        t1 = System.currentTimeMillis();
        if (t1 - t0 >= 1000)
        {
            fps = (double) frames * ((t1 - t0) / 1000.);
            t0 = t1;
            frames = 0;
        }
        textRenderer.setColor(1f, 0f, 0f, 1f);
        textRenderer.beginRendering(drawable.getWidth(), drawable.getHeight());
        textRenderer.draw(String.format("%2.2f",fps), 0, drawable.getHeight() - 20);
        textRenderer.endRendering();
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height)
    {
    }

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged)
    {
    }
    
}
