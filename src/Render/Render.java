package Render;

import Mesh.Mesh;
import Mesh.Vector;
import RayCasting.*;
import Voxels.VoxelBuffer;
import Voxels.VoxelModel;
import Voxels.VoxelReader;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.*;
import javax.media.opengl.glu.GLU;
import java.io.IOException;

import static javax.media.opengl.GL2.*;

public class Render implements GLEventListener
{
	Camera camera;
	GLU glu;
	GLProfile profile;
    public VoxelModel model;
	RenderCPU_Flat render;
    VoxelBuffer bufferModel;
    GLUT glut;
    Mesh mesh;
    public boolean MODEL_OF;

    public Render(GLProfile profile, Camera camera)
    {
        this.camera = camera;
        this.profile = profile;

        try
        {
//            mesh = new Mesh();
//            mesh.LoadFromFile("obj/tor.obj");
//            mesh.pos.z += mesh.size * 2;
            VoxelReader reader = new VoxelReader();
            model = reader.read("octree/tor1.octree");
            System.out.println("Finish read...");
            model.setPoint(new Vector(-0.5, -0.5, 0.5));
            render = new RenderCPU_Flat(model);
            glut = new GLUT();
        } catch (IOException e)
        {
            e.printStackTrace();
            System.exit(11);
        }
    }

    @Override
	public void init(GLAutoDrawable drawable)
	{
		GL2 gl = drawable.getGL().getGL2();
		glu = new GLU();
		gl.glClearColor(0f, 0f, 0f, 0f);
		gl.glMatrixMode(GL_PROJECTION);
		glu.gluPerspective(45, 4f / 3f, 0.001f, 400);
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
        gl.glEnable(GL_MULTISAMPLE);
        gl.glShadeModel(GL_SMOOTH);
        gl.glEnable(GL_LINE_SMOOTH);
        gl.glEnable(GL_BLEND);
        gl.glEnable(GL_CULL_FACE);
        gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        gl.glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);

        float mat_specular[]={0.7f,0.7f,0.7f,0.7f};
        float mat_shininess[]={500.0f};
        float light_position[]={200f, .0f, 200.0f, 20.0f};
        float white_light[]={1.0f,1.0f,1.0f,1.0f};

        gl.glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular, 0);
        gl.glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess, 0);
        gl.glLightfv(GL_LIGHT0, GL_POSITION, light_position, 0);
        gl.glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light, 0);
        gl.glLightfv(GL_LIGHT0, GL_SPECULAR, white_light, 0);
        gl.glEnable(GL_LIGHTING);
        gl.glEnable(GL_LIGHT0);

        gl.glEnable(GL_DEPTH_TEST);
    }

	@Override
	public void display(GLAutoDrawable drawable)
	{
		GL2 gl = drawable.getGL().getGL2();
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glDisable(GL_LIGHTING);
        gl.glDisable(GL_LIGHT0);

        camera.look(glu);

        if(!MODEL_OF)
            model.draw(gl, glut);

        render.draw(gl);

//        gl.glEnable(GL_LIGHTING);
//        gl.glEnable(GL_LIGHT0);

//        mesh.draw(gl);

		gl.glLoadIdentity();
	}

	@Override
	public void reshape(GLAutoDrawable glDrawable, int x, int y, int w, int h)
	{
        if (h == 0) h = 1;
        GL2 gl = glDrawable.getGL().getGL2();
        GLU glu = new GLU();
        gl.glViewport(0, 0, w, h);
        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f,(double) w / h, 0.001f, 600);
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();
    }

	@Override
	public void dispose(GLAutoDrawable drawable)
	{
	}
}