import Mesh.Vector;
import RayCasting.RenderGPU;
import Voxels.VoxelModel;
import Voxels.VoxelReader;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: thinker
 * Date: 21.11.12
 * Time: 1:15
 */
public class MainGPU
{
    public static void main(String[] args)
    {
        try
        {
            VoxelReader reader = new VoxelReader();
            VoxelModel model = reader.read("octree/torAv.octree");
            System.out.println("Finish read...");
            model.setPoint(new Vector(-0.5, -0.5, 0.5));
            RenderGPU render = new RenderGPU(model);
        } catch (IOException e)
        {
            e.printStackTrace();
            System.exit(11);
        }
    }
}
